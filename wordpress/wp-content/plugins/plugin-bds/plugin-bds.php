<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Plugin_BDS
 *
 * @wordpress-plugin
 * Plugin Name:       Plugin BDS Demo
 * Plugin URI:        http://example.com/plugin-bds-uri/
 * Description:       Đây chỉ là một plugin demo, mục đích để test
 * Version:           1.0.0
 * Author:            TrongHuu95
 * Author URI:        http://example.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       plugin-bds
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('PLUGIN_NAME_VERSION', '1.0.0');



/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-plugin-bds-activator.php
 */

define('JWT_AUTH_SECRET_KEY', 'demo-bds');
define('JWT_AUTH_CORS_ENABLE', true);
require_once plugin_dir_path(__FILE__) . 'includes/variables.php';
require_once plugin_dir_path(__FILE__) . 'includes/abstracts/index.php';
require_once plugin_dir_path(__FILE__) . 'includes/common/common-functions.php';
require_once plugin_dir_path(__FILE__) . 'includes/common/common-navigates.php';
require_once plugin_dir_path(__FILE__) . 'includes/common/common-methods.php';
require_once plugin_dir_path(__FILE__) . 'includes/common/common-https.php';
require_once plugin_dir_path(__FILE__) . 'includes/class-plugin-bds-activator.php';
require_once plugin_dir_path(__FILE__) . 'includes/class-plugin-bds-deactivator.php';
require_once plugin_dir_path(__FILE__) . 'admin/index.php';
require_once plugin_dir_path(__FILE__) . 'includes/restapi/plugin-bds-restapi.php';
require_once plugin_dir_path(__FILE__) . 'includes/entitys/index.php';
require_once plugin_dir_path(__FILE__) . 'includes/stores/index.php';
require_once plugin_dir_path(__FILE__) . 'includes/pages/index.php';

// add style and scripts
function register_init()
{
	$plugin_admin = new PluginBDSAdmin('new_style', '1.0.0');
	$plugin_admin->enqueue_styles();
	$plugin_admin->enqueue_scripts();
}
add_action('init', 'register_init');

//activation
register_activation_hook(__FILE__, 'Plugin_BDS_Activator::activate');
//deactivation
register_deactivation_hook(__FILE__, 'Plugin_BDS_Deactivator::deactivate');
//add menu
(new Pages())->addMenu();
add_action('rest_api_init', function () {
	$PluginBDSRestApi = new PluginBDSRestApi();
	$PluginBDSRestApi->register();
});

include 'includes/shortcodes/bds-shortcode.php';
include 'includes/scheduleds/index.php';
Variables::$cache_du_an = BDSDuAn::all();
Variables::$cache_kieu_nha = BDSKieuNha::all();

Variables::set_var(CommonFunctions::get_configs());
// require_once plugin_dir_path(__FILE__) . '';


function add_head_html()
{ ?>
	<div class="overlay"></div>
<?php }
add_action('admin_head', 'add_head_html');
