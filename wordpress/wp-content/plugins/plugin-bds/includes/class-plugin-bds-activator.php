<?php
// require_once plugin_dir_path(__FILE__) . 'includes/pages/index.php';

// require_once plugin_dir_path(__FILE__) . 'includes/stores/index.php';

class Plugin_BDS_Activator
{
	public static function activate()
	{
		self::create_table();
	}

	/**
	 * Create new table
	 */
	protected static function create_table()
	{
		Stores::init();
	}
}
