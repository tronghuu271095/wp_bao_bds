<?php
// tài liệu tham khảo: https://huynhtanmao.com/huong-dan-tao-rest-api-trong-wordpress/
//Fix lỗi 404
// On WPEngine and WP 4.9.2 I only had to update permalinks to get fresh, newly installed site to return v2 API calls. What I did:
// Create site
// Browse to http://yoursitename.wpengine.com/wp-json/wp/v2/posts
// get 404
// Go to admin, settings, permalinks, choose "Post Name"
// Click "Save Changes"
// Browse to http://yoursitename.wpengine.com/wp-json/wp/v2/posts
// success. page displays JSON response
class PluginBDSRestApi extends WP_REST_Controller
{
    public function register()
    {
        $namespace = 'api/v2';
        $path = 'thanh_vien';
        register_rest_route($namespace, '/' . $path, [
            array(
                'methods'             => 'GET',
                'callback'            => array($this, 'get'),
                'permission_callback' => array($this, 'permissions_check')

                // 'permission_callback' => '__return_true',
            )

        ]);
    }
    public function permissions_check($request)
    {
        return true; //current_user_can('wordpress');
    }

    public function get($request)
    {
        // Get sent data and set default value

        $result = BDSThanhVien::all();
        return rest_ensure_response($result);
    }
}
