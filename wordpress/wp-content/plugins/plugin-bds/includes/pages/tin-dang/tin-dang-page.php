<?php

/**
 * TinDang_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders

 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */

class TinDangPage
{
    function __construct()
    {
        $this->slug = NameTables::$tin_dang . '.php';
        add_action('admin_menu', array($this, 'addMenu'));
        wp_enqueue_script('tin-dang-page', plugin_dir_url(__FILE__) . '/tin-dang-page.js', array('jquery'));
    }

    function addMenu()
    {
        $page_title = 'Quản lý Tin Đăng';
        $menu_title = 'Tin Đăng';
        $capability = Variables::$capability;
        $callback = array($this, 'addPage');
        $icon = 'dashicons-rss';
        add_submenu_page('bds', $page_title, $menu_title, $capability,  $this->slug, $callback, $icon);
    }

    public function addPage()
    {
        $sid_site =  $_GET['id_site'] ?? -1;
        $sid_du_an =  $_GET['id_du_an'] ?? 1;
        $stab =  $_GET['tab'] ?? 1;

        if (isset($_POST['btn_apply'])) {
            $params = array(
                "id_du_an" => $_POST['build_id_du_an'],
                "tab" => $_POST['tab'],

            );
            if ($_POST['build_id_site'] > 0) {
                $params["id_site"] = $_POST['build_id_site'];
            }
            CommonNavigates::toPage_TinDang($params);
        }

        if (isset($_POST['tab_new'])) {
            $params = array(
                "id_du_an" => $_POST['build_id_du_an'],
            );
            if ($_POST['build_id_site'] > 0) {
                $params["id_site"] = $_POST['build_id_site'];
            }
            CommonNavigates::toPage_TinDang($params);
        }
        if (isset($_POST['tab_posted'])) {
            $params = array(
                "id_du_an" => $_POST['build_id_du_an'],
                "tab" => 2,

            );
            if ($_POST['build_id_site'] > 0) {
                $params["id_site"] = $_POST['build_id_site'];
            }
            CommonNavigates::toPage_TinDang($params);
        }
        if (isset($_POST['tab_cron'])) {
            $params = array(
                "id_du_an" => $_POST['build_id_du_an'],
                "tab" => 3,

            );
            if ($_POST['build_id_site'] > 0) {
                $params["id_site"] = $_POST['build_id_site'];
            }
            CommonNavigates::toPage_TinDang($params);
        }

        if (isset($_POST['btn_build'])) {
            $params = array(
                "id_du_an" => $_POST['build_id_du_an'],

            );
            if ($_POST['build_id_site'] > 0) {
                $params["id_site"] = $_POST['build_id_site'];
            }
            BDSTinDang::build_data_du_an($params);
            CommonNavigates::toPage_TinDang($params);
        }

        if (isset($_POST['btn_back'])) {
            CommonNavigates::toPage_TinDang();
        }

        if (isset($_POST['btn_update'])) {
            $item = BDSTinDang::find_one($_GET['upt']);
            $duan = BDSDuAn::find_one($item->id_du_an);

            $item->tieu_de = $_POST['upt_tieu_de'];
            $item->mo_ta = $_POST['upt_mo_ta'];
            $item->is_vip = $_POST['upt_is_vip'] ?? false;
            $item->gia = $_POST['upt_gia'];
            $item->dien_tich = $_POST['upt_dien_tich'];
            $item->id_site = intval($_POST['upt_id_site']);
            $item->id_thanh_vien = intval($_POST['upt_id_thanh_vien']);
            $item->dang_sau = $_POST['upt_dang_sau'];
            $item->ngay_dang = $_POST['upt_ngay_dang'];
            $item->ghi_chu = $_POST['upt_ghi_chu'];
            if ($duan->loai > 1) {
                $item->id_kieu_nha = $_POST['upt_kieu_nha'];
                $item->duong_vao = $_POST['upt_duong_vao'];
                $item->huong_nha = $_POST['upt_huong_nha'];
                $item->so_phong_ngu = $_POST['upt_so_phong_ngu'];
                $item->so_toilet = $_POST['upt_so_toilet'];
                $item->so_tang = $_POST['upt_so_tang'];
            }
            $item->save();
            $params = array(
                "upt" => $item->id,
            );

            CommonNavigates::toPage_TinDang($params);
        }

        if (isset($_POST['btn_duplicate'])) {
            $item = BDSTinDang::find_one($_GET['upt']);
            $duan = BDSDuAn::find_one($item->id_du_an);
            $item->tieu_de = $_POST['upt_tieu_de'];
            $item->mo_ta = $_POST['upt_mo_ta'];
            $item->is_vip = $_POST['upt_is_vip'] ?? false;
            $item->gia = $_POST['upt_gia'];
            $item->dien_tich = $_POST['upt_dien_tich'];
            $item->id_site = $_POST['upt_id_site'];
            $item->id_thanh_vien = $_POST['upt_id_thanh_vien'];
            $item->dang_sau = $_POST['upt_dang_sau'];
            $item->ngay_dang = $_POST['upt_ngay_dang'];
            $item->ghi_chu = $_POST['upt_ghi_chu'];
            if ($duan->loai > 1) {
                $item->id_kieu_nha = $_POST['upt_kieu_nha'];
                $item->duong_vao = $_POST['upt_duong_vao'];
                $item->huong_nha = $_POST['upt_huong_nha'];
                $item->so_phong_ngu = $_POST['upt_so_phong_ngu'];
                $item->so_toilet = $_POST['upt_so_toilet'];
                $item->so_tang = $_POST['upt_so_tang'];
            }
            $item_duplicate = $item->duplicate();
            if ($item_duplicate != null) {
                $params = array(
                    "upt" => $item_duplicate->id,
                );
                CommonFunctions::alert("Nhân bản thành công");
                CommonNavigates::toPage_TinDang($params);
            } else {
                CommonFunctions::alert("Nhân bản thất bại");
            }
        }
?>
<?php
        if (!isset($_GET['upt'])) {
            $query = BDSTinDang::query();
            if ($sid_site >= 1) {
                $query = $query->where("id_site",  $sid_site);
            }
            if ($sid_du_an >= 1) {
                $query = $query->where("id_du_an",  $sid_du_an);
            }
            switch ($stab) {
                case 2:
                    $result = $query->where_gte("id_tin",  1)->sort_by('ngay_tao')->order('DESC')->find();
                    break;
                case 3:
                    $result = $query->is_null("id_tin")->str_query('`ngay_dang` is not null && `ngay_dang` > CURDATE() ')->sort_by('ngay_tao')->order('DESC')->find();
                    break;
                default:
                    $result = $query->is_null("id_tin")->str_query('`ngay_dang` < now() OR `ngay_dang` is null')->sort_by('ngay_tao')->order('DESC')->find();
                    break;
            }

            echo  "
      <div class='wrap'>
           
            <form method='post'>
                    <h2>Danh sách Tin Đăng (<span class='rows_selected' id='select_count'>0 Selected</span>)</h2>
                    <div class='tab'>
                        <button class='tablinks " . ($stab == 1 ? 'active' : '') . " ' name='tab_new' id='tab_new' >Tin mới tạo</button>
                        <button class='tablinks " . ($stab == 2 ? 'active' : '') . " ' name='tab_posted' id='tab_posted'>Tin đã đăng</button>
                        <button class='tablinks " . ($stab == 3 ? 'active' : '') . " ' name='tab_cron' id='tab_cron'>Tin chờ đăng</button>
                    </div>
                    
                    <select id='build_id_du_an' name='build_id_du_an' aria-label='Default select example'>";
            foreach (Variables::$cache_du_an as $du_an) {
                echo "<option value='$du_an->id'" . ($sid_du_an == $du_an->id ? "selected" : "") . ">" . $du_an->ma_du_an . "</option>";
            }
            echo "
                    </select>
                    <select id='build_id_site' name='build_id_site' aria-label='Default select example'>
                    <option value= '-1'> Tất cả </option>";
            foreach (array_keys(Variables::$site) as $site) {
                echo "<option value='$site'" . ($sid_site == $site ? "selected" : "") . ">" . Variables::$site[$site] . "</option>";
            }
            echo "
                    </select>
                <input type='submit' name='btn_apply' class='btn btn-primary' value='Apply'>
                " . ($stab != 1 ? "" : "<input type='submit' name='btn_build' class='btn btn-primary' value='Build data'>") . "

                <div class='row'>   
                    <a type='button' id='btn_up_news' name='btn_up_news' class='btn btn-outline-primary pull-right'><i class='fas fa-upload'></i> Đăng tin</a>
                    <a type='button' id='btn_duplicate_news' name='btn_duplicate_news' class='btn btn-outline-danger pull-right'><i class='far fa-copy'></i> Nhân bản</a>
                    <a type='button' id='delete_records_tin_dang' name='delete_records_tin_dang' class='btn btn-outline-danger pull-right'><i class='far fa-trash-alt'></i> Xóa</a>
                
                </div>
            </form>

            <table class='table table-bordered' style='table-layout: fixed; width: 100%'>
               
                <thead>
                    <tr>
                        <th width='5%'><input type='checkbox' id='select_all'> </th>
                        <th width='15%'>Dự án</th>
                        <th width='15%'>Tiêu đề</th>
                        <th width='15%'>Địa chỉ</th>
                        <th width='15%'>Giá - Diện tích</th>
                        <th width='10%'>Ngày tạo</th>
                        <th width='10%'>Ngày đăng</th>
                        <th width='5%'>Link sửa</th>
                        <th width='5%'>Link xem</th>
                        <th width='25%'> Ghi chú</th>
                        <th width='5%'></th>
                    </tr>
                </thead>
                <tbody>

                   ";
            foreach ($result as $item) {
                $duan = $item->du_an();
                $thanh_vien = $item->thanh_vien();

                echo "
							<tr id='$item->id'>   
								<td width='5%'><input type='checkbox' class='emp_checkbox' data-emp-id='$item->id'></td></td>
                                <td width='15%' style='word-break:break-word' >" . $duan->ma_du_an .
                    " - <span>" . $item->name_site() . "</span>" .
                    "<p>" .  $thanh_vien->ten_dang_nhap . "</p>" .
                    "<p>" .  $thanh_vien->mat_khau . "</p>" .
                    "</td>                             
                                <td width='15%'>" . ($item->is_vip ? "<span  style='color: #e6c004d4;font-weight: 800;'>Vip </span>" : '') . $item->tieu_de . "</td>
                                <td width='15%'>$item->dia_chi</td>
                                <td width='15%'> <p> Giá: " . CommonFunctions::int_format_number($item->gia) . " VNĐ/m2</p>" . "<p> Diện tích: " . CommonFunctions::int_format_number($item->dien_tich) . " m2</p>" . "</td>
                                <td width='10%'>$item->ngay_tao</td>
                                <td width='10%'>$item->ngay_dang</td>
                                <td width='5%'>" . "<a href='$item->link_sua' target='_blank'>Link</a>" . "</td>
                                <td width='5%'>" . "<a href='$item->link_xem' target='_blank'>Link</a>" . "</td>
                                <td width='25%'>" . "[" . $item->text_trang_thai() . "]" . ($item->id_tin > 0 ? " (id: $item->id_tin)" : '') .
                    $item->ghi_chu . "</td>
                                <td width='5%'>
                                <a href='admin.php?page=$this->slug&upt=$item->id' class='btn btn-outline-primary'><i class='fas fa-edit mr-2'></i></a> 
							</tr>
							";
            }
            echo "
                </tbody>
            </table>
            <br>
            <br>
        </div>
      ";
        }
?>
        <?php
        if (isset($_GET['upt'])) {
            $upt_id = $_GET['upt'];
            $item = BDSTinDang::find_one($upt_id);
            $duan = $item->du_an();
            $thanh_viens = BDSThanhVien::all();
            $ngay_dang = $item->ngay_dang ? date("Y-m-d", strtotime($item->ngay_dang)) : null;
            echo "
                <div class='wrap'>
                    <form class='form-horizontal' action='' method='post'>
                    <div class='row'>
                        <button id='btn_back' name='btn_back' class='btn btn-danger pull-left'><i class='fas fa-arrow-left'></i> Trở về</button>
                        <div class='pull-right'>
                            <button id='btn_duplicate' name='btn_duplicate' class='btn btn-info'><i class='far fa-copy'></i> Nhân bản</button>
                            <button id='btn_update' name='btn_update'  class='btn btn-success'><i class='far fa-save'></i> Lưu</button>
                        </div>
                      
                    </div>
                    <h2>  Dự án: " . $duan->ma_du_an . "-" . ($duan->ten_du_an ?? 'Không xác định') . ($item->id_tin != null && $item->id_tin > 0 ? " (id: $item->id_tin)" : '') .  "</h2>
                    <div class='col-md-6'>

                        <div class='form-group'>
                            <label class='col-md-2 control-label' for='upt_tieu_de'>Tiêu đề</label>  
                            <div class='col-md-8'>
                                <input id='upt_tieu_de' " . ($item->id_tin > 0 ? 'readonly' : '') . " name='upt_tieu_de' type='text' placeholder='Nhập tiêu đề' class='form-control input-md' value='$item->tieu_de'>
                            </div>
                            <div class='col-md-2'>
                                <label class='checkbox-inline' for='upt_is_vip'>
                                    <input " . ($item->id_tin > 0 ? 'disabled' : '') . " type='checkbox' name='upt_is_vip' id='upt_is_vip' value=1 " . ($item->is_vip ? "checked" : "") . ">
                                    Is vip
                                </label>
                            </div>
                        </div>
                        
                        <div class='form-group'>
                            <label class='col-md-2 control-label' for='upt_mo_ta'>Mô tả</label>  
                            <div class='col-md-10'>
                                <textarea  id='upt_mo_ta' " . ($item->id_tin > 0 ? 'readonly' : '') . " name='upt_mo_ta' type='text' placeholder='Nhập mô tả' class='form-control input-md' rows='10'>$item->mo_ta</textarea>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label class='col-md-2 control-label' for='upt_mo_ta'>Địa chỉ</label>  
                            <div class='col-md-10'>
                                <input id='upt_dia_chi' " . ($item->id_tin > 0 ? 'readonly' : '') . " name='upt_dia_chi' type='text' placeholder='Nhập địa chỉ' class='form-control input-md' value='$item->dia_chi'>
                            </div>
                        </div>
                        
                        <div class='form-group'>
                            <label class='col-md-2 control-label' for='upt_dien_tich'>Diện tích (m2)</label>  
                            <div class='col-md-4'>
                                <input type='number' " . ($item->id_tin > 0 ? 'readonly' : '') . " id='upt_dien_tich' name='upt_dien_tich' type='text' placeholder='Nhập diện tích' class='form-control input-md' value='$item->dien_tich'>
                            </div>
                            <label class='col-md-2 control-label' for='upt_gia'>Giá (VND/m2)</label>  
                            <div class='col-md-4'>
                                <input type='number' " . ($item->id_tin > 0 ? 'readonly' : '') . " id='upt_gia' name='upt_gia' type='text' placeholder='Nhập giá' class='form-control input-md' value='$item->gia'> 
                            </div>
                        </div>
                        ";
            if ($duan->loai > 1) {
                echo "
                        <div class='form-group'>
                            <label class='col-md-2 control-label'></label>  
                            <div class='col-md-4'>
                                <strong> " . $duan->ten_loai() . "</strong>
                            </div>
                        </div>

                        <div class='form-group'>
                            
                            <label class='col-md-2 control-label'>Kiểu nhà</label>  
                            <div class='col-md-4'>
                                <select " . ($item->id_tin > 0 ? 'disabled' : '') . " id='upt_kieu_nha' name='upt_kieu_nha' aria-label='Default select example'>";
                foreach (Variables::$cache_kieu_nha as $kieu_nha) {
                    echo "<option value='$kieu_nha->id'" . ($kieu_nha->id == $item->id_kieu_nha ? "selected" : "") . ">" . $kieu_nha->ten_kieu_nha . "</option>";
                }
                echo "</select>   
                            </div>
                            <label class='col-md-2 control-label'>Đường vào (m)</label>  
                            <div class='col-md-4'>
                                <input " . ($item->id_tin > 0 ? 'readonly' : '') . " id='upt_duong_vao' name='upt_duong_vao' type='text' placeholder='Nhập đường vào (m)' class='form-control input-md' value='$item->duong_vao'>
                            </div>
                        </div>
                        
                        <div class='form-group'>
                            <label class='col-md-2 control-label' for='upt_huong_nha'>Hướng nhà</label>  
                            <div class='col-md-4'>
                                <select " . ($item->id_tin > 0 ? 'disabled' : '') . " id='upt_huong_nha' name='upt_huong_nha' aria-label='Default select example'>";
                foreach (array_keys(Variables::$huong_nha) as $huongnha) {
                    echo "<option value='$huongnha'" . ($item->huong_nha == $huongnha ? "selected" : "") . ">" . Variables::$huong_nha[$huongnha] . "</option>";
                }
                echo "
                                    </select>                        
                            </div>
                            <label class='col-md-2 control-label' for='upt_so_phong_ngu'>Số phòng ngũ</label>  
                            <div class='col-md-4'>
                                <input " . ($item->id_tin > 0 ? 'readonly' : '') . " id='upt_so_phong_ngu' name='upt_so_phong_ngu' type='text' placeholder='Nhập số phòng ngũ' class='form-control input-md' value='$item->so_phong_ngu'>
                            </div>

                            
                        </div>
                        
                        <div class='form-group'>
                        
                            <label class='col-md-2 control-label' for='upt_so_toilet'>Số toilet</label>  
                            <div class='col-md-4'>
                                <input " . ($item->id_tin > 0 ? 'readonly' : '') . " id='upt_so_toilet' name='upt_so_toilet' type='text' placeholder='Nhập số toilet' class='form-control input-md' value='$item->so_toilet'>
                            </div>

                            <label class='col-md-2 control-label' for='upt_so_tang'>Số tầng</label>  
                            <div class='col-md-4'>
                                <input " . ($item->id_tin > 0 ? 'readonly' : '') . " id='upt_so_tang' name='upt_so_tang' type='text' placeholder='Nhập số tầng' class='form-control input-md' value='$item->so_tang'>
                            </div>
                            
                        </div>";
            }
            echo "
                        <div class='form-group'>
                            <label class='col-md-2 control-label' for='upt_id_site'>Website</label>  
                            <div class='col-md-4'>
                                <select " . ($item->id_tin > 0 ? 'disabled' : '') . " id='upt_id_site' name='upt_id_site' aria-label='Default select example'>";
            foreach (array_keys(Variables::$site) as $site) {
                echo "<option value='$site'" . ($item->id_site == $site ? "selected" : "") . ">" . Variables::$site[$site] . "</option>";
            }
            echo "
                                </select>
                            </div>
                            <label class='col-md-2 control-label' for='upt_id_thanh_vien'>Tài khoản</label>  
                            <div class='col-md-4'>
                                <select " . ($item->id_tin > 0 ? 'disabled' : '') . " id='upt_id_thanh_vien'  name='upt_id_thanh_vien' aria-label='Default select example'>";
            foreach ($thanh_viens as $thanh_vien) {
                echo "<option value='$thanh_vien->id'" . ($item->id_thanh_vien == $thanh_vien->id ? "selected" : "") . ">  $thanh_vien->ho_ten ($thanh_vien->dien_thoai)</option>";
            }
            echo "
                                </select>
                            </div>
                        </div>
                       
                    </div>
                    <div class='col-md-6'>
                        <div class='form-group'>
                            <label class='col-md-2 control-label' for='upt_ghi_chu'>Đăng sau</label>  
                            <div class='col-md-4'>
                                <input type='number' " . ($item->id_tin > 0 ? 'readonly' : '') . " id='upt_dang_sau' name='upt_dang_sau' max=15 placeholder='Nhập đăng sau' class='form-control ' value='$item->dang_sau'></input>
                            </div>
                            <label class='col-md-2 control-label' for='upt_ngay_dang'>Ngày đăng</label>  
                            <div class='col-md-4'>
                                <input type='date' " . ($item->id_tin > 0 ? 'readonly' : '') . " id='upt_ngay_dang' name='upt_ngay_dang' class='form-control' value='$ngay_dang'></input>
                            </div>
                        </div>  
                        <div class='form-group'>
                            <label class='col-md-2 control-label' for='upt_ghi_chu'>Ghi chú</label>  
                            <div class='col-md-10'>
                                <textarea  id='upt_ghi_chu' name='upt_ghi_chu' type='text' placeholder='Nhập ghi chú' class='form-control input-md' rows='10'>$item->ghi_chu</textarea>
                            </div>
                        </div>  
                    </div>
                </form>
                </div>

                ";
        }
        ?>
<?php
    }
}
