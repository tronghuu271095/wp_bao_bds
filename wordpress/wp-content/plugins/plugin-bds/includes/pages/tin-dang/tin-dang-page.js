jQuery(document).on("click", "#select_all", function () {
  jQuery(".emp_checkbox").prop("checked", this.checked);
  jQuery("#select_count").html(
    jQuery("input.emp_checkbox:checked").length + " Selected"
  );
});

jQuery(document).on("click", ".emp_checkbox", function () {
  if (
    jQuery(".emp_checkbox:checked").length == jQuery(".emp_checkbox").length
  ) {
    jQuery("#select_all").prop("checked", true);
  } else {
    jQuery("#select_all").prop("checked", false);
  }
  jQuery("#select_count").html(
    jQuery("input.emp_checkbox:checked").length + " Selected"
  );
});
jQuery(document).on("click", "#delete_records_tin_dang", function () {
  var employee = [];
  jQuery(".emp_checkbox:checked").each(function () {
    employee.push(jQuery(this).data("emp-id"));
  });
  if (employee.length <= 0) {
    alert("Please select records.");
  } else {
    WRN_PROFILE_DELETE =
      "Bạn có chắc muốn xóa các tin đăng chọn " +
      (employee.length > 1 ? "these" : "this") +
      " row?";
    var checked = confirm(WRN_PROFILE_DELETE);
    if (checked == true) {
      jQuery.ajax({
        type: "POST",
        url: Variables.path_admin_ajax,
        data: {
            action: 'bds_del_tin_dang',
            ids: employee
        },
        beforeSend: function(){
          CommonMethods.start_loading();
        },
        complete: function(){
          CommonMethods.stop_loading();
        },
        success: function (response) {
          for (var i = 0; i < employee.length; i++) {
            jQuery("#" + employee[i]).remove();
          }
          alert("Xóa thành công");
        },
      });
     
    }
  }
});

jQuery(document).on("click", "#btn_up_news", function () {
  var employee = [];
  jQuery(".emp_checkbox:checked").each(function () {
    employee.push(jQuery(this).data("emp-id"));
  });
  if (employee.length <= 0) {
    alert("Please select records.");
  } else {
    WRN_PROFILE_DELETE =
      "Bạn có chắc muốn đăng các tin đăng chọn " +
      (employee.length > 1 ? "these" : "this") +
      " row?";
    var checked = confirm(WRN_PROFILE_DELETE);
    if (checked == true) {
      jQuery.ajax({
        type: "POST",
        url: Variables.path_admin_ajax,
        data: {
            action: 'bds_up_news',
            ids: employee
        },
        beforeSend: function(){
          CommonMethods.start_loading();
        },
        complete: function(){
          CommonMethods.stop_loading();
        },
         success:function(output){
            alert("Đăng tin thành công");
            location.reload();
         },
         error: function(errorThrown){
             alert(errorThrown);
         }
      });
    }
  }
});
jQuery(document).on("click", "#btn_duplicate_news", function () {
  var employee = [];
  jQuery(".emp_checkbox:checked").each(function () {
    employee.push(jQuery(this).data("emp-id"));
  });
  if (employee.length <= 0) {
    alert("Please select records.");
  } else {
    WRN_PROFILE_DELETE =
      "Bạn có chắc muốn nhân bản các tin đăng chọn " +
      (employee.length > 1 ? "these" : "this") +
      " row?";
    var checked = confirm(WRN_PROFILE_DELETE);
    if (checked == true) {
      jQuery.ajax({
        type: "POST",
        url: Variables.path_admin_ajax,
        data: {
            action: 'bds_duplicate_news',
            ids: employee
        },
        beforeSend: function(){
          CommonMethods.start_loading();
        },
        complete: function(){
          CommonMethods.stop_loading();
        },
         success:function(output){
            alert("Nhân bản thành công");
            location.reload();
         },
         error: function(errorThrown){
             alert(errorThrown);
         }
      });
    }
  }
});
