<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders

 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */

class VietTatPage
{
    function __construct()
    {
        $this->slug = NameTables::$viet_tat . '.php';
        add_action('admin_menu', array($this, 'addMenu'));
    }
    function addMenu()
    {
        $page_title = 'Quản lý viết tắt';
        $menu_title = 'Viết tắt';
        $capability = Variables::$capability;
        $callback = array($this, 'addPage');
        $icon = 'dashicons-rss';
        add_submenu_page('bds', $page_title, $menu_title, $capability, $this->slug, $callback, $icon);
    }

    public function addPage()
    {
        if (isset($_POST['newsubmit'])) {
            (new BDSVietTat(array(
                "tu_goc" => $_POST['new_tu_goc'],
                "tu_thay_the" => $_POST['new_tu_thay_the'],
            )))->save();

            CommonNavigates::toPage_VietTat();
        }
        if (isset($_POST['uptsubmit'])) {
            (new BDSVietTat(array(
                "id" => $_GET['upt'],
                "tu_goc" => $_POST['upt_tu_goc'],
                "tu_thay_the" => $_POST['upt_tu_thay_the'],
            )))->save();
            // $this->datae->dataStore->update($uptdata, $wheredata);
            CommonNavigates::toPage_VietTat(); // chuyển đến trang này
        }
        if (isset($_GET['del'])) {
            (new BDSVietTat(array(
                "id" => $_GET['del'],
            )))->delete();

            CommonNavigates::toPage_VietTat();
        }

?>

        <div class="wrap">
            <h2>Danh sách Viết tắt</h2>
            <table class="wp-list-table widefat striped">
                <thead>
                    <tr>
                        <th width="10%">Id</th>
                        <th width="15%">Từ gốc</th>
                        <th width="30%">Từ thay thế</th>
                    </tr>
                </thead>
                <tbody>
                    <form action="" method="post">
                        <tr>
                            <td><input type="text" value="AUTO_GENERATED" disabled></td>
                            <td><input type="text" id="new_tu_goc" name="new_tu_goc"></td>
                            <td><input type="text" id="new_tu_thay_the" name="new_tu_thay_the"></td>
                            <td><button id="newsubmit" name="newsubmit" type="submit">INSERT</button></td>
                        </tr>
                    </form>
                    <?php
                    $result = BDSVietTat::all();
                    foreach ($result as $item) {
                        echo "
							<tr>
								<td width='10%'>$item->id</td>
								<td width='15%'>$item->tu_goc</td>
								<td width='30%'>$item->tu_thay_the</td>
								<td width='25%'><a href='admin.php?page=$this->slug&upt=$item->id'><button type='button'>UPDATE</button></a> <a href='admin.php?page=$this->slug&del=$item->id'><button type='button'>DELETE</button></a></td>
							</tr>
							";
                    }
                    ?>
                </tbody>
            </table>
            <br>
            <br>
            <?php
            if (isset($_GET['upt'])) {
                $upt_id = $_GET['upt'];
                $item = BDSVietTat::find_one($upt_id);
                echo "
                <table class='wp-list-table widefat striped'>
                <thead>
                	<tr>
                		<th width='10%'>Id</th>
                		<th width='15%'>Từ gốc</th>
                		<th width='10%'>Từ thay thế</th>
                		<th width='25%'>Actions</th>
                	</tr>
                </thead>
                <tbody>
                	<form action='' method='post'>
                	<tr>
                		<th width='10%'>$item->id <input type='hidden' id='uptid' name='uptid' value='$item->id'></td>
                		<th width='15%'><input type='text' id='upt_tu_goc' name='upt_tu_goc' value='$item->tu_goc'></td>
                		<th width='30%'><input type='text' id='upt_tu_thay_the' name='upt_tu_thay_the' value='$item->tu_thay_the'></td>
                		<th width='25%'><button id='uptsubmit' name='uptsubmit' type='submit'>UPDATE</button> <a href='admin.php?page=$this->slug'><button type='button'>CANCEL</button></a></td>
                	</tr>
                	</form>
                </tbody>
                </table>";
            }
            ?>
        </div>
<?php
    }
}
