<?php
include "viet-tat/viet-tat-page.php";
include "kieu-nha/kieu-nha-page.php";
include "link-site/link-site-page.php";
include "logs-link-site/logs-link-site-page.php";
include "mo-ta-them/mo-ta-them-page.php";
include "thuoc-tinh/thuoc-tinh-page.php";
include "du-an/du-an-page.php";
include "tin-dang/tin-dang-page.php";
include "thanh-vien/thanh-vien-page.php";
include "config/config-page.php";

class Pages
{
    function __construct()
    {
        add_action("admin_menu", array($this, "addMenuBDS"));
    }
    function addMenu()
    {
        // new VietTatPage();
        // new KieuNhaPage();
        // new LogsLinkSitePage();
        // new MoTaThemPage();
        new ConfigPage();
        new ThanhVienPage();
        new DuAnPage();
        new TinDangPage();
    }

    function addMenuBDS()
    {
        $capability = Variables::$capability;
        add_menu_page("BDS", "BDS", $capability, "bds",  array($this, "menubds"), "");
    }
    function menubds()
    {
        if (isset($_POST["btn_check_trang_thai_tin"])) {
            CommonFunctions::start_loading();
            try {
                $data = BDSTinDang::query()->where_gte("id_tin", 1)->where("trang_thai", 1)->limit(Variables::$max_check_trang_thai_tin)->sort_by('ngay_tao')->order('DESC')->find();
                foreach ($data as $item) {
                    $item->cap_nhat_trang_thai();
                }
            } catch (\Throwable $th) {
            }
            CommonFunctions::stop_loading();
        }
        if (isset($_POST["btn_cap_nhat_balance"])) {
            CommonFunctions::start_loading();
            $res = BDSThanhVienSite::update_balance_by_site(1);
            CommonFunctions::alert($res ? "Cập nhật tiền thành công" : "Cập nhật tiền thất bại");
            CommonFunctions::stop_loading();
        }

        if (isset($_POST["btn_clear_chrome"])) {
            CommonFunctions::start_loading();
            CommonMethods::clear_chrome();
            CommonFunctions::stop_loading();
        }

        if (isset($_POST["btn_check_link_du_an"])) {
            CommonFunctions::start_loading();
            $res = BDSDuAn::check_links_duan();
            CommonFunctions::alert($res ? "Check link dự án thành công" : "Check link dự án thất bại");
            CommonFunctions::stop_loading();
        }

        if (isset($_POST["btn_posted"])) {
            CommonFunctions::start_loading();
            $data = BDSTinDang::query()->is_null("id_tin")->find();
            foreach ($data as $item) {
                $item->post();
            }
            CommonFunctions::stop_loading();
        }


?>
        <div class="wrap">
            <h2>Danh sách chức năng chính</h2>
            <form method="post">
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" name="btn_clear_chrome" value="Clear chorme">
                    <input type="submit" class="btn btn-primary" name="btn_check_trang_thai_tin" value="Cập nhật trạng thái tin đăng">
                    <input type="submit" class="btn btn-primary" name="btn_check_link_du_an" value="Check link dự án">
                    <input type="submit" class="btn btn-primary" name="btn_posted" value="Đăng các tin đã tạo">

                </div>
                <label>batdongsan.com.vn</label>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" name="btn_cap_nhat_balance" value="Cập nhật tiền ">
                </div>
                <div class="form-group">
                    <select id="kieu_nap" name="kieu_nap" aria-label="Default select example">
                        <?php foreach (array_keys(Variables::$loai_tai_khoan) as $tk) {
                            echo "<option value=$tk>" . Variables::$loai_tai_khoan[$tk] . "</option>";
                        }
                        ?>
                    </select>
                    <input type="number" name="so_tien" value="10000">
                    <input type="submit" class="btn btn-primary" name="btn_nap_tien" value="Nạp tiền">
                </div>

            </form>
        </div>
<?php
    }
}
