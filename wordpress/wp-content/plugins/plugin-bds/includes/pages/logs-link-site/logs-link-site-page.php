<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders

 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */

class LogsLinkSitePage
{
    function __construct()
    {
        $this->slug = NameTables::$logs_link_site . '.php';
        add_action('admin_menu', array($this, 'addMenu'));
    }
    function addMenu()
    {
        $page_title = 'Quản lý logs link';
        $menu_title = 'Logs link';
        $capability = Variables::$capability;
        $callback = array($this, 'addPage');
        $icon = 'dashicons-rss';
        add_submenu_page('bds', $page_title, $menu_title, $capability, $this->slug, $callback, $icon);
    }

    public function addPage()
    {

        if (isset($_GET['del'])) {
            (new BDSLogsLinkSite(array(
                "id" => $_GET['del'],
            )))->delete();

            CommonNavigates::toPage_LogsLinkSite();
        }

?>
        <div class="wrap">
            <h2>Danh sách Logs link</h2>
            <table class="wp-list-table widefat striped">
                <thead>
                    <tr>
                        <th width="10%">Id</th>
                        <th width="15%">Link website</th>
                        <th width="30%">Người đăng</th>
                        <th width="15%">Giá</th>
                        <th width="15%">Diện tích</th>
                        <th width="15%">Ngày kiểm tra</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $result = BDSLogsLinkSite::all();
                    foreach ($result as $item) {
                        echo "
							<tr>
								<td width='10%'>$item->id</td>
								<td width='15%'>$item->id_du_an</td>
								<td width='30%'>$item->ten_nguoi_dang - $item->text_dien_thoai</td>
								<td width='15%'>$item->text_gia</td>
                                <td width='15%'>$item->text_dien_tich</td>
                                <td width='15%'>$item->ngay_tao</td>
								<td width='25%'><a href='admin.php?page=$this->slug&del=$item->id'><button type='button'>DELETE</button></a></td>
							</tr>
							";
                    }
                    ?>
                </tbody>
            </table>
            <br>
            <br>
        </div>
<?php
    }
}
