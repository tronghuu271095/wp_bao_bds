<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders

 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */

class ThuocTinhPage
{
    function __construct()
    {
        $this->slug = NameTables::$thuoc_tinh . '.php';
        add_action('admin_menu', array($this, 'addMenu'));
    }
    function addMenu()
    {
        $page_title = 'Quản lý Thuộc tính';
        $menu_title = 'Thuộc tính';
        $capability = Variables::$capability;
        $callback = array($this, 'addPage');
        $icon = 'dashicons-rss';
        add_submenu_page('bds', $page_title, $menu_title, $capability, $this->slug, $callback, $icon);
    }
    public function addPage()
    {
        if (isset($_POST['newsubmit'])) {
            (new BDSThuocTinh(array(
                "ten_thuoc_tinh" => $_POST['new_ten_thuoc_tinh'],
                "ma_thuoc_tinh" => $_POST['new_ma_thuoc_tinh'],
                "noi_dung" => $_POST['new_noi_dung'],
            )))->save();

            CommonNavigates::toPage_ThuocTinh();
        }
        if (isset($_POST['uptsubmit'])) {
            (new BDSThuocTinh(array(
                "id" => $_GET['upt'],
                "ten_thuoc_tinh" => $_POST['upt_ten_thuoc_tinh'],
                "ma_thuoc_tinh" => $_POST['upt_ma_thuoc_tinh'],
                "noi_dung" => $_POST['upt_noi_dung'],
            )))->save();
            // $this->datae->dataStore->update($uptdata, $wheredata);
            CommonNavigates::toPage_ThuocTinh(); // chuyển đến trang này
        }
        if (isset($_GET['del'])) {
            (new BDSThuocTinh(array(
                "id" => $_GET['del'],
            )))->delete();

            CommonNavigates::toPage_ThuocTinh();
        }

?>

        <div class="wrap">
            <h2>Danh sách Thuộc tính</h2>
            <table class="wp-list-table widefat striped">
                <thead>
                    <tr>
                        <th width="10%">Id</th>
                        <th width="15%">Tên thuộc tính</th>
                        <th width="10%">Mã thuộc tính</th>
                        <th width="10%">Nội dung</th>
                    </tr>
                </thead>
                <tbody>
                    <form action="" method="post">
                        <tr>
                            <td><input type="text" value="AUTO_GENERATED" disabled></td>
                            <td><input type="text" id="new_ten_thuoc_tinh" name="new_ten_thuoc_tinh"></td>
                            <td><input type="text" id="new_ma_thuoc_tinh" name="new_ma_thuoc_tinh"></td>
                            <td><input type="text" id="new_noi_dung" name="new_noi_dung"></td>
                            <td><button id="newsubmit" name="newsubmit" type="submit">INSERT</button></td>
                        </tr>
                    </form>
                    <?php
                    $result = BDSThuocTinh::all();
                    foreach ($result as $item) {
                        echo "
							<tr>
								<td width='10%'>$item->id</td>
								<td width='15%'>$item->ten_thuoc_tinh</td>
								<td width='20%'>$item->ma_thuoc_tinh</td>
								<td width='10%'>$item->noi_dung</td>
								<td width='25%'><a href='admin.php?page=$this->slug&upt=$item->id'><button type='button'>UPDATE</button></a> <a href='admin.php?page=$this->slug&del=$item->id'><button type='button'>DELETE</button></a></td>
							</tr>
							";
                    }
                    ?>
                </tbody>
            </table>
            <br>
            <br>
            <?php
            if (isset($_GET['upt'])) {
                $upt_id = $_GET['upt'];
                $item = BDSThuocTinh::find_one($upt_id);
                echo "
                <table class='wp-list-table widefat striped'>
                <thead>
                	<tr>
                		<th width='10%'>Id</th>
                		<th width='15%'>Tên thuộc tính</th>
                		<th width='10%'>Mã thuộc tính</th>
                		<th width='10%'>Nội dung</th>	
                		<th width='25%'>Actions</th>
                	</tr>
                </thead>
                <tbody>
                	<form action='' method='post'>
                	<tr>
                		<th width='10%'>$item->id <input type='hidden' id='uptid' name='uptid' value='$item->id'></td>
                		<th width='15%'><input type='text' id='upt_ten_thuoc_tinh' name='upt_ten_thuoc_tinh' value='$item->ten_thuoc_tinh'></td>
                		<th width='10%'><input type='text' id='upt_ma_thuoc_tinh' name='upt_ma_thuoc_tinh' value='$item->ma_thuoc_tinh'></td>
                		<th width='10%'><input type='text' id='upt_noi_dung' name='upt_noi_dung' value='$item->noi_dung'></td>
                        <th width='25%'><button id='uptsubmit' name='uptsubmit' type='submit'>UPDATE</button> <a href='admin.php?page=$this->slug'><button type='button'>CANCEL</button></a></td>
                	</tr>
                	</form>
                </tbody>
                </table>";
            }
            ?>
        </div>
<?php
    }
}
