<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders

 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */

class KieuNhaPage
{
    // protected $table_name = 'site';
    // protected $name_page = 'KieuNha';
    // protected $icon = 'dashicons-rss';
    function __construct()
    {
        $this->slug = NameTables::$kieu_nha . '.php';
        add_action('admin_menu', array($this, 'addMenu'));
    }

    function addMenu()
    {
        $page_title = 'Quản lý Kiểu nhà';
        $menu_title = 'Kiểu nhà';
        $capability = Variables::$capability;
        $callback = array($this, 'addPage');
        $icon = 'dashicons-rss';
        add_submenu_page('bds', $page_title, $menu_title, $capability,  $this->slug, $callback, $icon);
    }



    public function addPage()
    {
        if (isset($_GET['upt']) && isset($_POST['index_ketcau'])) {
            $item_id = $_GET['upt'];
            $index_ketcau = $_POST["index_ketcau"]; // được truyền qua data
            $item =  BDSKieuNha::find_one($item_id);
            if (isset($item)) {
                $item->del_ket_cau($index_ketcau);
                CommonNavigates::toPage_KieuNha();
            }
        }
        if (isset($_POST['newsubmit'])) {
            (new BDSKieuNha(array(
                "ten_kieu_nha" => $_POST['new_ten_kieu_nha'],
                "text_so_phong" => $_POST['new_text_so_phong'],
                "text_so_tang" => $_POST['new_text_so_tang'],
                "text_so_toilet" => $_POST['new_text_so_toilet'],
            )))->save();

            CommonNavigates::toPage_KieuNha();
        }
        if (isset($_POST['uptsubmit'])) {
            (new BDSKieuNha(array(
                "id" => $_GET['upt'],
                "ten_kieu_nha" => $_POST['upt_ten_kieu_nha'],
                "text_so_phong" => $_POST['upt_text_so_phong'],
                "text_so_tang" => $_POST['upt_text_so_tang'],
                "text_so_toilet" => $_POST['upt_text_so_toilet'],
                "text_ket_cau" => $_POST['upt_text_ket_cau']
            )))->save();

            // $this->datae->dataStore->update($uptdata, $wheredata);
            // CommonNavigates::toPage_KieuNha(); // chuyển đến trang này
        }
        if (isset($_GET['del'])) {
            (new BDSKieuNha(array(
                "id" => $_GET['del'],
            )))->delete();

            CommonNavigates::toPage_KieuNha();
        }

?>
        <div class="wrap">
            <h2>Danh sách Kiểu nhà</h2>
            <table class="wp-list-table widefat striped">
                <thead>
                    <tr>
                        <th width="10%">Id</th>
                        <th width="20%">Kiểu nhà</th>
                        <th width="15%">Txt số phòng</th>
                        <th width="15%">Txt số tầng</th>
                        <th width="15%">Txt số toilet</th>
                    </tr>
                </thead>
                <tbody>
                    <form action="" method="post">
                        <tr>
                            <td><input type="text" value="AUTO_GENERATED" disabled></td>
                            <td><input type="text" id="new_ten_kieu_nha" name="new_ten_kieu_nha"></td>
                            <td><input type="text" id="new_text_so_phong" name="new_text_so_phong"></td>
                            <td><input type="text" id="new_text_so_tang" name="new_text_so_tang"></td>
                            <td><input type="text" id="new_text_so_toilet" name="new_text_so_toilet"></td>
                            <td><button id="newsubmit" name="newsubmit" type="submit">INSERT</button></td>
                        </tr>
                    </form>
                    <?php
                    $result = BDSKieuNha::all();

                    foreach ($result as $item) {
                        echo "
							<tr>                                
								<td width='10%'>$item->id</td>
								<td width='20%'>$item->ten_kieu_nha</td>
								<td width='15%'>$item->text_so_phong</td>
								<td width='15%'>$item->text_so_tang</td>
                                <td width='15%'>$item->text_so_toilet</td>
								<td width='25%'><a href='admin.php?page=$this->slug&upt=$item->id'><button type='button'>UPDATE</button></a> <a href='admin.php?page=$this->slug&del=$item->id'><button type='button'>DELETE</button></a></td>
							</tr>
							";
                    }
                    ?>
                </tbody>
            </table>
            <br>
            <br>
            <?php
            if (isset($_GET['upt'])) {
                $upt_id = $_GET['upt'];
                $item = BDSKieuNha::find_one($upt_id);
                $ket_caus = $item->ket_cau();
                echo "
                <form action='' method='post'>

				<table class='wp-list-table widefat striped'>
				<thead>
					<tr>
						<th width='10%'>Id</th>
						<th width='15%'>Kiểu nhà</th>
						<th width='15%'>Txt số phòng</th>
						<th width='15%'>Txt số tầng</th>	
                        <th width='15%'>Txt số toilet</th>						
						<th width='25%'>Actions</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td width='10%'>$item->id <input type='hidden' id='uptid' name='uptid' value='$item->id'></td>
						<td width='15%'><input type='text' id='upt_ten_kieu_nha' name='upt_ten_kieu_nha' value='$item->ten_kieu_nha'></td>
						<td width='15%'><input type='text' id='upt_text_so_phong' name='upt_text_so_phong' value='$item->text_so_phong'></td>
						<td width='15%'><input type='text' id='upt_text_so_tang' name='upt_text_so_tang' value='$item->text_so_tang'></td>
                        <td width='15%'><input type='text' id='upt_text_so_toilet' name='upt_text_so_toilet' value='$item->text_so_toilet'></td>
                        <td width='25%'><button id='uptsubmit' name='uptsubmit' type='submit'>UPDATE</button> <a href='admin.php?page=$this->slug'><button type='button'>CANCEL</button></a></td>
					</tr>
				</tbody>
				</table>

                <br>
                <br>
                ";
                echo "
                <div class='wrap'>
                <h2>Danh sách kết cấu</h2>
                <textarea name='upt_text_ket_cau' rows='7' >
                $item->text_ket_cau
                </textarea>
                </div>
                </form>

                ";
            }
            ?>
        </div>
<?php
    }
}
