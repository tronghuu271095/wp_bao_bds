<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders

 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */

class MoTaThemPage
{
    function __construct()
    {
        $this->slug = NameTables::$mo_ta_them . '.php';
        add_action('admin_menu', array($this, 'addMenu'));
    }
    function addMenu()
    {
        $page_title = 'Quản lý Mô tả thêm';
        $menu_title = 'Mô tả thêm';
        $capability = Variables::$capability;
        $callback = array($this, 'addPage');
        $icon = 'dashicons-rss';
        add_submenu_page('bds', $page_title, $menu_title, $capability, $this->slug, $callback, $icon);
    }
    public function addPage()
    {
        if (isset($_POST['newsubmit'])) {
            (new BDSMoTaThem(array(
                "noi_dung" => $_POST['new_noi_dung'],
                "nhom" => $_POST['new_nhom'],
                "is_top" => $_POST['new_is_top'],
                "is_ban_dat" => $_POST['new_is_ban_dat']

            )))->save();

            CommonNavigates::toPage_MoTaThem();
        }
        if (isset($_POST['uptsubmit'])) {
            (new BDSMoTaThem(array(
                "id" => $_GET['upt'],
                "noi_dung" => $_POST['upt_noi_dung'],
                "nhom" => $_POST['upt_nhom'],
                "is_top" => $_POST['upt_is_top'],
                "is_ban_dat" => $_POST['upt_is_ban_dat']

            )))->save();
            // $this->datae->dataStore->update($uptdata, $wheredata);
            CommonNavigates::toPage_MoTaThem(); // chuyển đến trang này
        }
        if (isset($_GET['del'])) {
            (new BDSMoTaThem(array(
                "id" => $_GET['del'],
            )))->delete();

            CommonNavigates::toPage_MoTaThem();
        }

?>

        <div class="wrap">
            <h2>Danh sách Mô tả thêm</h2>
            <table class="wp-list-table widefat striped">
                <thead>
                    <tr>
                        <th width="10%">Id</th>
                        <th width="15%">Nội dung</th>
                        <th width="10%">Nhóm</th>
                        <th width="10%">Trên đầu</th>
                        <th width="10%">Loại Bán đất</th>

                    </tr>
                </thead>
                <tbody>
                    <form action="" method="post">
                        <tr>
                            <td><input type="text" value="AUTO_GENERATED" disabled></td>
                            <td><input type="text" id="new_noi_dung" name="new_noi_dung"></td>
                            <td><input type="text" id="new_nhom" name="new_nhom"></td>
                            <td><input type="text" id="new_is_top" name="new_is_top"></td>
                            <td><input type="text" id="new_is_ban_dat" name="new_is_ban_dat"></td>

                            <td><button id="newsubmit" name="newsubmit" type="submit">INSERT</button></td>
                        </tr>
                    </form>
                    <?php
                    $result = BDSMoTaThem::all();
                    foreach ($result as $item) {
                        echo "
							<tr>
								<td width='10%'>$item->id</td>
								<td width='15%'>$item->noi_dung</td>
								<td width='20%'>$item->nhom</td>
								<td width='10%'>$item->is_top</td>
                                <td width='10%'>$item->is_ban_dat</td>
								<td width='25%'><a href='admin.php?page=$this->slug&upt=$item->id'><button type='button'>UPDATE</button></a> <a href='admin.php?page=$this->slug&del=$item->id'><button type='button'>DELETE</button></a></td>
							</tr>
							";
                    }
                    ?>
                </tbody>
            </table>
            <br>
            <br>
            <?php
            if (isset($_GET['upt'])) {
                $upt_id = $_GET['upt'];
                $item = BDSMoTaThem::find_one($upt_id);
                echo "
                <table class='wp-list-table widefat striped'>
                <thead>
                	<tr>
                		<th width='10%'>Id</th>
                		<th width='15%'>Nội dung</th>
                		<th width='10%'>Nhóm</th>
                		<th width='10%'>Trên đầu</th>	
                        <th width='10%'>Loại Bán đất</th>											
                		<th width='25%'>Actions</th>
                	</tr>
                </thead>
                <tbody>
                	<form action='' method='post'>
                	<tr>
                		<th width='10%'>$item->id <input type='hidden' id='uptid' name='uptid' value='$item->id'></td>
                		<th width='15%'><input type='text' id='upt_noi_dung' name='upt_noi_dung' value='$item->noi_dung'></td>
                		<th width='10%'><input type='text' id='upt_nhom' name='upt_nhom' value='$item->nhom'></td>
                		<th width='10%'><input type='text' id='upt_is_top' name='upt_is_top' value='$item->is_top'></td>
                        <th width='10%'><input type='text' id='upt_is_ban_dat' name='upt_is_ban_dat' value='$item->upt_is_ban_dat'></td>

                        <th width='25%'><button id='uptsubmit' name='uptsubmit' type='submit'>UPDATE</button> <a href='admin.php?page=$this->slug'><button type='button'>CANCEL</button></a></td>
                	</tr>
                	</form>
                </tbody>
                </table>";
            }
            ?>
        </div>
<?php
    }
}
