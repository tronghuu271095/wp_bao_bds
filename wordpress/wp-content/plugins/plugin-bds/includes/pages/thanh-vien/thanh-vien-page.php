<?php

/**
 * ThanhVien_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders

 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */

class ThanhVienPage
{

    function __construct()
    {
        $this->slug = NameTables::$thanh_vien . '.php';
        add_action('admin_menu', array($this, 'addMenu'));
        wp_enqueue_script('thanh-vien-page', plugin_dir_url(__FILE__) . '/thanh-vien-page.js', array('jquery'));
    }

    function addMenu()
    {
        $page_title = 'Quản Lý Thành Viên';
        $menu_title = 'Thành Viên';
        $capability = Variables::$capability;
        $callback = array($this, 'addPage');
        $icon = 'dashicons-rss';
        add_submenu_page('bds', $page_title, $menu_title, $capability,  $this->slug, $callback, $icon);
    }

    public function addPage()
    {
        if (isset($_POST['btn_add'])) {
            CommonNavigates::toPage_ThanhVien(array("upt" => -1));
        }
        if (isset($_POST['btn_back'])) {
            CommonNavigates::toPage_ThanhVien();
        }
        if (isset($_POST['btn_update_balance'])) {
            CommonFunctions::start_loading();
            try {
                $upt_id = $_GET['upt'] > 0 ? $_GET['upt'] : 1;
                $res = BDSThanhVienSite::update_balance_by_thanh_vien($upt_id);
                CommonFunctions::alert($res ? "Cập nhật thành công" : "Cập nhật thất bại");
                if ($res) {
                    $params = array(
                        "upt" => $upt_id,
                    );
                    CommonNavigates::toPage_ThanhVien($params);
                }
            } catch (\Throwable $th) {
                //throw $th;
            }
            CommonFunctions::stop_loading();
        }
        if (isset($_POST['btn_update'])) {
            $upt_id = $_GET['upt'] > 0 ? $_GET['upt'] : 1;
            $item = BDSThanhVien::find_one($upt_id);
            if ($item->id  <= 0) {
                $item->id = null;
            }
            $item->ho_ten = $_POST['upt_ho_ten'];
            $item->email = $_POST['upt_email'];
            $item->dien_thoai = $_POST['upt_dien_thoai'];
            $item->dia_chi = $_POST['upt_dia_chi'];
            $item->active = $_POST['upt_active'];
            $id_save = $item->save();
            if ($upt_id <= 0) {
                $upt_id = $id_save;
            }

            if ($upt_id > 0) {
                foreach (array_keys(Variables::$site) as $site) {
                    $accounts = BDSThanhVienSite::query()->where('id_thanh_vien', $upt_id)->where('id_site', $site)->find();
                    $account = new BDSThanhVienSite();
                    if ($accounts != null && count($accounts) >= 1) {
                        $account = $accounts[0];
                    }
                    if ($account->id  <= 0) {
                        $account->id = null;
                    }
                    $account->id_thanh_vien = $upt_id;
                    $account->id_site = $site;
                    $account->ten_dang_nhap = $_POST["upt_ten_dang_nhap_$site"];
                    $account->mat_khau = $_POST["upt_mat_khau_$site"];
                    $account->save();
                }
            }

            CommonNavigates::toPage_ThanhVien();
        }
?>
<?php
        if (!isset($_GET['upt'])) {
            $result = BDSThanhVien::query()->sort_by('ho_ten')->order('DESC')->find();
            echo  "
      <div class='wrap'>
            <h2>Danh Sách Dự Án (<span class='rows_selected' id='select_count'>0 Selected</span>)</h2>
            <form method='post'>
                <input type='submit' id='btn_add' name='btn_add' class='btn btn-primary' value='Thêm'>
                <div class='row'>   
                    <a type='button' id='delete_records_thanh_vien' name='delete_records_thanh_vien' class='btn btn-outline-danger pull-right'><i class='far fa-trash-alt'></i> Xóa</a>
                </div>
            </form>

            <table class='table table-bordered' style='table-layout: fixed; width: 100%'>
               
                <thead>
                    <tr>
                        <th width='5%'><input type='checkbox' id='select_all'> </th>
                        <th width='10%'>Họ tên</th>
                        <th width='15%'>Email</th>
                        <th width='15%'>Điện thoại</th>
                        <th width='15%'>Địa chỉ</th>
                        <th width='5%'>Active</th>
                        <th width='5%'></th>
                    </tr>
                </thead>
                <tbody>

                   ";
            foreach ($result as $item) {
                echo "
							<tr id='$item->id'>   
								<td width='5%'><input type='checkbox' class='emp_checkbox' data-emp-id='$item->id'></td></td>
                                <td width='10%'>$item->ho_ten</span>" .
                    "</td>                             
                                <td width='15%'>$item->email</td>
                                <td width='15%'>$item->dien_thoai</td>
                                <td width='15%' >$item->dia_chi</td>
                                <td width='5%'>" . ($item->active ? 'Hoạt động' : 'Đã tắt') . "</td>
                                <td width='5%'>
                                <a href='admin.php?page=$this->slug&upt=$item->id' class='btn btn-outline-primary'><i class='fas fa-edit mr-2'></i></a> 
							</tr>
							";
            }
            echo "
                </tbody>
            </table>
            <br>
            <br>
        </div>
      ";
        }
?>
        <?php
        if (isset($_GET['upt'])) {
            $upt_id = $_GET['upt'] > 0 ? $_GET['upt'] : 1;
            $item = new BDSThanhVien();
            if ($_GET['upt'] > 0) {
                $item = BDSThanhVien::find_one($upt_id);
            }
            echo "
                <div class='wrap'>
                    <form class='form-horizontal' action='' method='post'>
                        <div class='row'>
                            <button id='btn_back' name='btn_back' class='btn btn-danger pull-left'><i class='fas fa-arrow-left'></i> Trở về</button>
                            <div class='pull-right'>
                                " . ($item->id > 0 ? "<button id='btn_update_balance' name='btn_update_balance' class='btn btn-info'><i class='fas fa-wrench'></i> Cập nhật số tiền</button>" : '') . "
                                <button id='btn_update' name='btn_update'  class='btn btn-success'><i class='far fa-save'></i> Lưu</button>
                            </div>
                        </div>
                        <div class='col-md-6'>
                        
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_ho_ten'>Họ tên</label>  
                                <div class='col-md-8'>
                                    <input id='upt_ho_ten' name='upt_ho_ten' type='text' placeholder='Nhập họ tên' class='form-control input-md' value='$item->ho_ten'>
                                </div>
                                <div class='col-md-2'>
                                    <label class='checkbox-inline' for='upt_active'>
                                        <input type='checkbox' name='upt_active' id='upt_active' value=1 " . ($item->active ? "checked" : "") . ">
                                        Active
                                    </label>
                                </div>
                            </div>
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_email'>Email</label>  
                                <div class='col-md-10'>
                                    <input id='upt_email' name='upt_email' type='text' placeholder='Nhập email' class='form-control input-md' value='$item->email'>
                                </div>
                            
                            </div>
                            
                        
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_dien_thoai'>Điện thoại</label>  
                                <div class='col-md-10'>
                                    <input id='upt_dien_thoai' name='upt_dien_thoai' type='text' placeholder='Nhập điện thoại' class='form-control input-md' value='$item->dien_thoai'>
                                </div>                           
                            </div>

                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_dia_chi'>Địa chỉ</label>  
                                <div class='col-md-10'>
                                    <input id='upt_dia_chi' name='upt_dia_chi' type='text' placeholder='Nhập địa chỉ' class='form-control input-md' value='$item->dia_chi'>
                                </div>                           
                            </div>
                           
                        </div>
                        <div class='col-md-6'>
                            ";
            foreach (array_keys(Variables::$site) as $site) {
                $accounts = BDSThanhVienSite::query()->where('id_thanh_vien', $upt_id)->where('id_site', $site)->find();
                $account = new BDSThanhVienSite();
                if ($accounts != null && count($accounts) >= 1) {
                    $account = $accounts[0];
                }
                if ($site != 3) {
                    echo "
                <div class='row'>   
                    <p class='control-label'> Website: <a target='_blank' href=http://" . Variables::$site[$site] . ">" . Variables::$site[$site] . "</a></p>  
                    <p class='control-label'> Số tiền:  " . ($account->balance ? CommonFunctions::int_format_price($account->balance) : 'Chưa cập nhật') . "</p>  
                    <p class='control-label'>$account->ghi_chu" . "</p>  

                </div>";
                }
                if ($site == 3) {
                    echo "
                <div class='row'>   
                    <p class='control-label'> Website: <a target='_blank' href=http://" . Variables::$site[$site] . ">" . Variables::$site[$site] . "</a></p>  
                    <p class='control-label'> <strong>Đăng miễn phí</strong></p>  

                </div>";
                }
                echo " <div class='form-group'>
                    <label class='col-md-2 control-label' for='upt_ten_dang_nhap_$site'>Tên đăng nhập</label>  
                    <div class='col-md-4'>
                        <input id='upt_ten_dang_nhap_$site' name='upt_ten_dang_nhap_$site' type='text' placeholder='Tên đăng nhập' class='form-control input-md' value='$account->ten_dang_nhap'>
                    </div>  
                    <label class='col-md-2 control-label' for='upt_mat_khau_$site'>Mật khẩu</label>  
                    <div class='col-md-4'>
                        <input id='upt_mat_khau_$site' name='upt_mat_khau_$site' type='text' placeholder='Mật khẩu' class='form-control input-md' value='$account->mat_khau'>
                    </div>                            
                </div>
                
                ";
            }
            echo "
                        </div>
                        
                    </form>
                </div>

                ";
        }
        ?>
<?php
    }
}
