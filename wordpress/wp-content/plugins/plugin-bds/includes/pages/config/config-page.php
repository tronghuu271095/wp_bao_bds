<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders

 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */

class ConfigPage
{
    // protected $table_name = 'config';
    // protected $name_page = 'Config';
    // protected $icon = 'dashicons-rss';
    function __construct()
    {
        $this->slug = NameTables::$config . '.php';
        add_action('admin_menu', array($this, 'addMenu'));
    }

    function addMenu()
    {
        $page_title = 'Quản Lý Cấu Hình';
        $menu_title = 'Cấu Hình';
        $capability = Variables::$capability;
        $callback = array($this, 'addPage');
        $icon = 'dashicons-rss';
        add_submenu_page('bds', $page_title, $menu_title, $capability,  $this->slug, $callback, $icon);
    }

    public function addPage()
    {

        if (isset($_POST['btn_update'])) {
            $data = CommonFunctions::get_configs();
            $data['path_api'] = $_POST['upt_path_api'];
            $data['max_length_tieu_de'] = intval($_POST['upt_max_length_tieu_de'] ?? 99);
            $data['max_length_mo_ta'] = intval($_POST['upt_max_length_mo_ta'] ?? 3000);
            $data['max_build'] = intval($_POST['upt_max_build'] ?? 10);
            $data['max_check_trang_thai_tin'] = intval($_POST['upt_max_check_trang_thai_tin'] ?? 10);
            $data['step_gia'] = floatval($_POST['upt_step_gia'] ?? 0.1);
            $data['step_dien_tich'] = floatval($_POST['upt_dien_tich'] ?? 0.1);
            CommonFunctions::set_configs($data);
            Variables::set_var(CommonFunctions::get_configs());
            CommonNavigates::toPage_Config();
        }
?>
<?php
        $data = CommonFunctions::get_configs();
        echo "
                <div class='wrap'>

                    <form class='form-horizontal' action='' method='post'>
                        <div class='row'>
                            <button id='btn_back' name='btn_back' class='btn btn-danger pull-left'><i class='fas fa-arrow-left'></i> Trở về</button>
                            <button id='btn_update' name='btn_update' class='btn btn-success pull-right'><i class='far fa-save'></i> Lưu</button>
                        </div>
                        <div class='col-md-6'>
                        </div>                        
                    
                    <h2>Thông tin cấu hình</h2>
                        <div class='form-group'>
                            <label class='col-md-2 control-label' for='upt_path_api'>Đường dẫn api</label>  
                            <div class='col-md-10'>
                                <input id='upt_path_api' name='upt_path_api' type='text' placeholder='Đường dẫn api' class='form-control input-md' value='" . $data['path_api'] . "'>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label class='col-md-2 control-label' for='upt_max_length_tieu_de'>Chiều dài tiêu đề tối đa</label>  
                            <div class='col-md-4'>
                                <input type='number' id='upt_max_length_tieu_de' name='upt_max_length_tieu_de' type='text' placeholder='Chiều dài tiêu đề' class='form-control input-md' value='" . $data['max_length_tieu_de'] . "'>
                            </div>
                            <label class='col-md-2 control-label' for='upt_max_length_mo_ta'>Chiều dài mô tả tối đa</label>  
                            <div class='col-md-4'>
                                <input type='number' id='upt_max_length_mo_ta' name='upt_max_length_mo_ta' type='text' placeholder='Chiều dài mô tả' class='form-control input-md' value='" . $data['max_length_mo_ta'] . "'>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label class='col-md-2 control-label' for='upt_max_build'>Build tối đa / 1 lần</label>  
                            <div class='col-md-4'>
                                <input type='number' id='upt_max_build' name='upt_max_build' type='text' placeholder='Đường dẫn api' class='form-control input-md' value='" . $data['max_build'] . "'>
                            </div>
                            <label class='col-md-2 control-label' for='upt_max_check_trang_thai_tin'>Check trạng thái tin tối đa / 1 lần</label>  
                            <div class='col-md-4'>
                                <input type='number' id='upt_max_check_trang_thai_tin' name='upt_max_check_trang_thai_tin' type='text' placeholder='Đường dẫn api' class='form-control input-md' value='" . $data['max_check_trang_thai_tin'] . "'>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label class='col-md-2 control-label' for='upt_step_gia'>Bước nhảy giá (triệu)</label>  
                            <div class='col-md-4'>
                                <input type='number' step=0.1 id='upt_step_gia' name='upt_step_gia' type='text' placeholder='Đường dẫn api' class='form-control input-md' value='" . $data['step_gia'] . "'>
                            </div>
                            <label class='col-md-2 control-label' for='upt_step_dien_tich'>Bước nhảy diện tích (m2)</label>  
                            <div class='col-md-4'>
                                <input type='number' step=0.1 id='upt_step_dien_tich' name='upt_step_dien_tich' type='text' placeholder='Đường dẫn api' class='form-control input-md' value='" . $data['step_dien_tich'] . "'>
                            </div>
                        </div>
                    </form>
                </div>
                </div>

                ";

?>
<?php
    }
}
