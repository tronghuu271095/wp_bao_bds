<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders

 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */

class DuAnPage
{
    // protected $table_name = 'du_an';
    // protected $name_page = 'DuAn';
    // protected $icon = 'dashicons-rss';
    function __construct()
    {
        $this->slug = NameTables::$du_an . '.php';
        add_action('admin_menu', array($this, 'addMenu'));
        wp_enqueue_script('du-an-page', plugin_dir_url(__FILE__) . '/du-an-page.js', array('jquery'));
    }

    function addMenu()
    {
        $page_title = 'Quản lý Dự án';
        $menu_title = 'Dự án';
        $capability = Variables::$capability;
        $callback = array($this, 'addPage');
        $icon = 'dashicons-rss';
        add_submenu_page('bds', $page_title, $menu_title, $capability,  $this->slug, $callback, $icon);
    }

    public function addPage()
    {
        if (isset($_POST['btn_add'])) {
            CommonNavigates::toPage_DuAn(array("upt" => -1));
        }
        if (isset($_POST['btn_back'])) {
            CommonNavigates::toPage_DuAn();
        }

        if (isset($_POST['btn_up_drive'])) {
            if (CommonMethods::is_not_null($_GET['upt']) && $_GET['upt'] > 0 && CommonMethods::is_not_null($_POST['upt_url_drive'])) {
                $item = BDSDuAn::find_one($_GET['upt']);
                $item->url_drive = $_POST['upt_url_drive'];
                CommonFunctions::start_loading();
                $res =  $item->up_drive();
                CommonFunctions::stop_loading();
                CommonFunctions::alert($res ? "Up ảnh thành công" : "Up ảnh thất bại");
            } else {
                CommonFunctions::alert("Dự án chưa đưcọ tạo hoặc url không được bỏ trống");
            }
        }

        if (isset($_POST['btn_update'])) {
            $item = BDSDuAn::find_one($_GET['upt']);
            $item->ma_du_an = $_POST['upt_ma_du_an'];
            $item->ten_du_an = $_POST['upt_ten_du_an'];
            $item->loai = $_POST['upt_loai'];
            $item->ten_duong = $_POST['upt_ten_duong'];
            $item->set_text_vi_tris($_POST['upt_text_vi_tri']);
            $item->url_drive = $_POST['upt_url_drive'];
            $item->text_gia = $_POST['upt_text_gia'];
            $item->active = $_POST['upt_active'];

            if (!CommonFunctions::string_is_not_null($item->text_gia)) {
                $item->text_gia_dat_tho_cu = $_POST['upt_text_gia_dat_tho_cu'];
                $item->text_gia_dat_khac = $_POST['upt_text_gia_dat_khac'];
            } else {
                $item->text_gia_dat_tho_cu = null;
                $item->text_gia_dat_khac = null;
            }
            $item->text_dien_tich = $_POST['upt_text_dien_tich'];
            if (!CommonFunctions::string_is_not_null($item->text_dien_tich)) {
                $item->text_dien_tich_dat_tho_cu = $_POST['upt_text_dien_tich_dat_tho_cu'];
                $item->text_dien_tich_dat_khac = $_POST['upt_text_dien_tich_dat_khac'];
            } else {
                $item->text_dien_tich_dat_tho_cu = null;
                $item->text_dien_tich_dat_khac = null;
            }
            if (CommonFunctions::string_is_not_null($_POST['upt_text_chieu_ngang'])) {
                $item->text_chieu_ngang = $_POST['upt_text_chieu_ngang'];
            }
            if (CommonFunctions::string_is_not_null($_POST['upt_text_tieu_de'])) {
                $item->set_text_tieu_des($_POST['upt_text_tieu_de']);
            }
            if (CommonFunctions::string_is_not_null($_POST['upt_link_checks'])) {
                $item->set_link_checks($_POST['upt_link_checks']);
            }
            $tien_ichs = array();
            foreach (Variables::$group_tien_ich as $group) {
                $text_tien_ich = $_POST["upt_tien_ich_$group"];
                if (CommonFunctions::string_is_not_null($text_tien_ich)) {
                    $tien_ichs[$group] = CommonFunctions::string_to_array($text_tien_ich, "\n");
                }
            }
            $item->text_tien_ich = json_encode($tien_ichs);

            $item->save();
            CommonNavigates::toPage_DuAn();
        }
?>
<?php
        if (!isset($_GET['upt'])) {
            $result = BDSDuAn::query()->sort_by('ma_du_an')->order('DESC')->find();
            echo  "
      <div class='wrap'>
            <h2>Danh Sách Dự Án (<span class='rows_selected' id='select_count'>0 Selected</span>)</h2>
            <form method='post'>
                <input type='submit' id='btn_add' name='btn_add' class='btn btn-primary' value='Thêm'>
                <div class='row'>  
                    <a type='button' id='up_drive_records_du_an' name='up_drive_records_du_an' class='btn btn-outline-danger pull-right'><i class='fas fa-cloud-upload-alt'></i> Up hình</a>
 
                    <a type='button' id='delete_records_du_an' name='delete_records_du_an' class='btn btn-outline-danger pull-right'><i class='far fa-trash-alt'></i> Xóa</a>
                </div>
            </form>

            <table class='table table-bordered' style='table-layout: fixed; width: 100%'>
               
                <thead>
                    <tr>
                        <th width='5%'><input type='checkbox' id='select_all'> </th>
                        <th width='10%'>Dự án</th>
                        <th width='15%'>Tên đường</th>
                        <th width='15%'>Url Drive</th>
                        <th width='15%'>Giá (triệu)</th>
                        <th width='15%'>Diện tích (m2)</th>
                        <th width='15%'>Chiều ngang</th>
                        <th width='5%'>Active</th>
                        <th width='5%'></th>
                    </tr>
                </thead>
                <tbody>

                   ";
            foreach ($result as $item) {
                echo "
							<tr id='$item->id'>   
								<td width='5%'><input type='checkbox' class='emp_checkbox' data-emp-id='$item->id'></td></td>
                                <td width='10%'>" . $item->ma_du_an .
                    " - <span>" . ($item->ten_du_an ?? 'Chưa cập nhật') . "</span>" .
                    "</td>                             
                                <td width='15%'>$item->ten_duong</td>
                                <td width='15%' style='word-break:break-word'>$item->url_drive</td>
                                <td width='15%' style='word-break:break-word'>" . ($item->text_gia ? "<p>$item->text_gia</p>" : "<p>Đất thổ cư: $item->text_gia_dat_tho_cu</p><p>Đất khác: $item->text_gia_dat_khac</p>") . "</td>
                                <td width='15%' style='word-break:break-word'>" . ($item->text_dien_tich ? "<p>$item->text_dien_tich</p>" : "<p>Đất thổ cư: $item->text_dien_tich_dat_tho_cu</p><p>Đất khác: $item->text_dien_tich_dat_khac</p>") . "</td>
                                <td width='15%' style='word-break:break-word'>$item->text_chieu_ngang</td>
                                <td width='5%'>" . ($item->active ? 'Hoạt động' : 'Đã tắt') . "</td>
                                <td width='5%'>
                                <a href='admin.php?page=$this->slug&upt=$item->id' class='btn btn-outline-primary'><i class='fas fa-edit mr-2'></i></a> 
							</tr>
							";
            }
            echo "
                </tbody>
            </table>
            <br>
            <br>
        </div>
      ";
        }
?>
        <?php
        if (isset($_GET['upt'])) {
            $upt_id = $_GET['upt'] > 0 ? $_GET['upt'] : 1;
            $item = BDSDuAn::find_one($upt_id);

            if ($_GET['upt'] <= 0) {
                $item->id = null;
                $item->ma_du_an = null;
                $item->ten_du_an = null;
            }
            echo "
                <div class='wrap'>
                    <form class='form-horizontal' action='' method='post'>
                        <div class='row'>
                            <button id='btn_back' name='btn_back' class='btn btn-danger pull-left'><i class='fas fa-arrow-left'></i> Trở về</button>
                            <button id='btn_update' name='btn_update' class='btn btn-success pull-right'><i class='far fa-save'></i> Lưu</button>
                        </div>
                        <div class='col-md-6'>
                        
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_ma_du_an'>Mã dự án</label>  
                                <div class='col-md-10'>
                                    <input id='upt_ma_du_an' name='upt_ma_du_an' type='text' placeholder='placeholder' class='form-control input-md' value='$item->ma_du_an'>
                                </div>
                            
                            </div>
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_ten_du_an'>Tên dự án</label>  
                                <div class='col-md-10'>
                                    <input id='upt_ten_du_an' name='upt_ten_du_an' type='text' placeholder='placeholder' class='form-control input-md' value='$item->ten_du_an'>
                                </div>
                            
                            </div>
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_loai'>Loại</label>  
                                <div class='col-md-8'>
                                    <select id='upt_loai' name='upt_loai' aria-label='Default select example'>";
            foreach (array_keys(Variables::$loai) as $loai) {
                echo "<option value='$loai'" . ($loai == $item->loai ? "selected" : "") . ">" . Variables::$loai[$loai] . "</option>";
            }
            echo "
                                    </select>
                                                    </div>
                                <div class='col-md-2'>
                                    <label class='checkbox-inline' for='upt_active'>
                                        <input type='checkbox' name='upt_active' id='upt_active' value=1 " . ($item->active ? "checked" : "") . ">
                                        Active
                                    </label>
                                </div>
                            </div>
                        
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_ten_duong'>Tên đường</label>  
                                <div class='col-md-10'>
                                    <input id='upt_ten_duong' name='upt_ten_duong' type='text' placeholder='placeholder' class='form-control input-md' value='$item->ten_duong'>
                                </div>                           
                            </div>
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_text_vi_tri'> Vị trí</label>  
                                <div class='col-md-10'>
                                    <textarea  id='upt_text_vi_tri' name='upt_text_vi_tri' type='text' placeholder='placeholder' class='form-control input-md' rows='3'>" . $item->get_text_vi_tris() . "</textarea>
                                </div>
                            </div>   
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_url_drive'>Url Drive</label>  
                                <div class='col-md-8'>
                                    <input id='upt_url_drive' name='upt_url_drive' type='text' placeholder='placeholder' class='form-control input-md' value='$item->url_drive'>
                                </div>
                                <div class='col-md-2'>
                                    <button id='btn_up_drive' name='btn_up_drive' class='btn btn-success pull-right input-md'><i class='fas fa-cloud-upload-alt'></i> Up hình</button>
                                </div>
                                
                            </div>
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_text_gia'>Giá (triệu)</label>  
                                <div class='col-md-10'>
                                    <input id='upt_text_gia' name='upt_text_gia' type='text' placeholder='placeholder' class='form-control input-md' value='$item->text_gia'>
                                </div>
                            </div>
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_text_gia_dat_tho_cu'>Giá đất thổ cư (triệu)</label>  
                                <div class='col-md-2'>
                                    <input  id='upt_text_gia_dat_tho_cu' name='upt_text_gia_dat_tho_cu' type='text' placeholder='placeholder' class='form-control input-md' value='$item->text_gia_dat_tho_cu'>
                                </div>
                                <label class='col-md-2 control-label' for='upt_text_dat_gia_khac'>Giá đất khác</label>  
                                <div class='col-md-2'>
                                    <input  id='upt_text_dat_gia_khac' name='upt_text_dat_gia_khac' type='text' placeholder='placeholder' class='form-control input-md' value='$item->text_gia_dat_khac'> 
                                </div>
                            </div>  
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_text_dien_tich'> Diện tích (m2)</label>  
                                <div class='col-md-10'>
                                    <input id='upt_text_dien_tich' name='upt_text_dien_tich' type='text' placeholder='placeholder' class='form-control input-md' value='$item->text_dien_tich'>
                                </div>
                            </div>
                           
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_text_dien_tich_dat_tho_cu'>Diện tích đất thổ cư (m2)</label>  
                                <div class='col-md-2'>
                                    <input  id='upt_text_dien_tich_dat_tho_cu' name='upt_text_dien_tich_dat_tho_cu' type='text' placeholder='placeholder' class='form-control input-md' value='$item->text_dien_tich_dat_tho_cu'>
                                </div>
                                <label class='col-md-2 control-label' for='upt_text_dat_gia_khac'>Diện tích đất khác</label>  
                                <div class='col-md-2'>
                                    <input  id='upt_text_dat_gia_khac' name='upt_text_dat_gia_khac' type='text' placeholder='placeholder' class='form-control input-md' value='$item->text_dien_tich_dat_khac'> 
                                </div>
                            </div>   
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_text_chieu_ngang'> Chiều ngang (m)</label>  
                                <div class='col-md-10'>
                                    <input id='upt_text_chieu_ngang' name='upt_text_chieu_ngang' type='text' placeholder='placeholder' class='form-control input-md' value='$item->text_chieu_ngang'>
                                </div>
                            </div>
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_text_tieu_de'>Tiêu đề</label>  
                                <div class='col-md-10'>
                                    <textarea  id='upt_text_tieu_de' name='upt_text_tieu_de' type='text' placeholder='placeholder' class='form-control input-md' rows='10'>" . $item->get_text_tieu_des() . "</textarea>
                                </div>                           
                            </div>
                            <div class='form-group'>
                                <label class='col-md-2 control-label' for='upt_link_checks'>Link checks</label>  
                                <div class='col-md-10'>
                                    <textarea  id='upt_link_checks' name='upt_link_checks' type='text' placeholder='placeholder' class='form-control input-md' rows='4'>" . $item->get_link_checks() . "</textarea>
                                </div>                           
                            </div>
                        </div>
                        <div class='col-md-6'>
                            
                            <div class='row'>   
                                <label class='col-md-2 control-label pull-left' for='upt_text_tien_ich'>Tiện ích</label>  
                            </div>
                            ";
            $tien_ichs = $item->tien_ichs();
            foreach (Variables::$group_tien_ich as $group) {
                echo "
                <div class='form-group'>
                    <label class='col-md-2 control-label' for='upt_tien_ich_$group'>Group $group</label>  
                    <div class='col-md-10'>
                        <textarea  id='upt_tien_ich_$group' name='upt_tien_ich_$group' type='text' placeholder='placeholder' class='form-control input-md' rows='5'>" . (CommonFunctions::array_to_string($tien_ichs[$group], "\n") ?? '') . "</textarea>
                    </div>                           
                </div>
                ";
            }
            echo "
                        </div>
                        
                    </form>
                </div>

                ";
        }
        ?>
<?php
    }
}
