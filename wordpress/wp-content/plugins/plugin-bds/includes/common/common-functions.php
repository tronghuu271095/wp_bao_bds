<?php

/**
 *  book common functions.
 *
 * @package \Common
 */
class CommonFunctions
{

    public static function start_loading()
    {
        echo '<script type="text/javascript">CommonMethods.start_loading()</script>';
    }
    public static function stop_loading()
    {
        echo '<script type="text/javascript">CommonMethods.stop_loading()</script>';
    }
    public static function alert($output, $with_script_tags = true)
    {
        $js_code = 'alert(' . json_encode($output, JSON_HEX_TAG) .
            ');';
        if ($with_script_tags) {
            $js_code = '<script type="text/javascript">' . $js_code . '</script>';
        }
        echo $js_code;
    }

    public static function console_log($output, $with_script_tags = true)
    {
        // // nguyencuongcs 20210712: 
        // // - console log làm admin console log ra lỗi dư data, chỉ uncomment return bên dưới khi cần dùng 
        // // - làm các chức năng js ở giao diện bị lỗi, treo. Vd: Add to cart ở product list loop, quickview
        // return;
        // nguyencuongcs: todo: need to load and check environment is not production
        self::console_log_execute($output, $with_script_tags);
    }

    public static function console_log_even_production($output, $with_script_tags = true)
    {
        self::console_log_execute($output, $with_script_tags);
    }

    private static function console_log_execute($output, $with_script_tags = true)
    {
        $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) .
            ');';
        if ($with_script_tags) {
            $js_code = '<script>' . $js_code . '</script>';
        }
        echo $js_code;
    }

    //========================== function string ========================
    public static function string_to_array(string $input, string $separator = " "): array
    {
        return explode($separator, $input);
    }

    public static function string_to_array_number(string $input, string $separator = " "): array
    {
        return array_map('intval', static::string_to_array($input, $separator));
    }



    public static function string_to_md5(string $input): string
    {
        return md5($input);
    }

    public static function string_is_not_null($input)
    {
        return  $input != null && strlen($input) > 0 && $input !== false && !empty($input);
    }

    public static function string_to_number(string $input): int
    {
        return intval($input);
    }

    public static function string_to_ucfirst(string $input): string
    {
        return ucfirst($input);
    }

    public static function string_fix_text($input): string
    {

        try {
            $input = preg_replace('/ +/', " ", $input);
            $input = preg_replace('/\.;/', ".", $input);
            $input = preg_replace('/,,/', ",", $input);
            $input = str_replace("muốn muốn", "muốn", $input);
            $input = str_replace("cần cần", "cần", $input);
            $input = str_replace("muốn cần", "cần", $input);
            $input = str_replace("cần muốn", "muốn", $input);
            $input = str_replace("cần thiếu", "thiếu", $input);
            $input = trim($input);
        } catch (\Throwable $th) {
            static::console_log($th);
        }
        return $input;
    }
    // =========================== funtion int
    public static function int_round(int $input, int $exponent = 2, int $def = -1): int
    {
        try {
            $round = pow(10, $exponent);
            return round($input * $round) / $round;
        } catch (\Throwable $th) {
            return $def;
        }
    }

    public static function int_format_price(float $input, $precision = 0, string $def = ""): string
    {
        if ($input < 1000) {
            // Anything less than a million
            $n_format = number_format($input);
        } else if ($input < 1000000) {
            // Anything less than a million
            $n_format = number_format($input / 1000, $precision) . ' nghìn';
        } else if ($input < 1000000000) {
            // Anything less than a billion
            $n_format = number_format($input / 1000000, $precision) . ' triệu';
        } else if ($input < 1000000000000) {
            // At least a billion
            $n_format = number_format($input / 1000000000, $precision) . ' tỷ';
        } else {
            // At least a billion
            $n_format = number_format($input / 1000000000000, $precision) . ' nghìn tỷ';
        }

        return $n_format;
    }

    public static function int_format_number(float $input): string
    {
        return number_format($input);
    }
    /// function array
    public static function array_to_string($array, string $separator = " "): string
    {
        try {
            return implode($separator, $array);
        } catch (\Throwable $th) {
            //throw $th;
        }
        return "";
    }
    public static function array_group_by(array $data, string $property = 'id')
    {
        $result = array();
        foreach ($data as $element) {
            $result[$element[$property]][] = $element;
        }
        return $result;
    }
    public static function array_to_json(array $data)
    {
        return  json_decode(json_encode($data), true);
    }

    public static function array_clone(array $data): array
    {

        $new_array = array();
        $new_array = $data;
        return $new_array;
    }
    public static function array_new_rand(array $array, int $num = 1): array
    {
        $keys = array_rand($array, $num);
        if ($num == 1) {
            return array($array[$keys]);
        }
        $results = [];
        foreach ($keys as $key) {
            $results[] = $array[$key];
        }

        return $results;
    }

    public static function array_rand_item(array $data)
    {
        $key = array_rand($data);
        return $data[$key];
    }

    public static function array_min(array $data)
    {
        return min($data);
    }

    public static function array_max(array $data)
    {
        return max($data);
    }

    public static function array_rand_index(array $data): int
    {
        return array_rand($data);
    }

    public static function array_remove_at(array $data, int $index): array
    {
        try {
            $clone_array = $data;
            unset($clone_array[$index]);
            return $clone_array;
        } catch (\Throwable $th) {
            return $data;
        }
    }

    public static function array_sort_string(array $data, bool $is_asc = true): array
    {
        $new_array = $data;
        $is_asc ? sort($data, SORT_STRING) : rsort($data, SORT_STRING);
        return $new_array;
    }
    public static function array_shuffle(array $data): array
    {
        $new_array = $data;
        shuffle($new_array);
        return $new_array;
    }

    public static function array_unique(array $data): array
    {
        return array_unique($data);
    }

    // function date
    public static function date_add_date(DateTime $data, DateInterval $interval): DateTime
    {
        $data->add($interval);
        return $data;
    }

    public static function get_configs(): array
    {
        try {
            $path_file = ABSPATH . 'wp-content/plugins/plugin-bds/config.json';
            $json_data = file_get_contents($path_file);
            return json_decode($json_data, true);
        } catch (Exception $ex) {
            CommonFunctions::console_log($ex);
        }
        return array();
    }

    public static function set_configs(array $data): bool
    {
        try {
            $path_file = ABSPATH . 'wp-content/plugins/plugin-bds/config.json';
            file_put_contents($path_file, json_encode($data));
            return true;
        } catch (Exception $ex) {
            CommonFunctions::console_log($ex);
        }
        return false;
    }
}
