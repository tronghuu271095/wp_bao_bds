<?php

/**
 *  book common functions.
 *
 * @package \Common
 */
class NameTables
{
    static $viet_tat = 'viet_tat';
    static $site = 'site';
    static $config = 'config';
    static $kieu_nha = 'kieu_nha';
    static $account = 'account';
    static $loai = 'loai';
    static $link_site = 'link_site';
    static $logs_link_site = 'logs_link_site';
    static $schedule = 'schedule';
    static $mo_ta_them = 'mo_ta_them';
    static $du_an = 'du_an';
    static $thuoc_tinh = 'thuoc_tinh';
    static $tin_dang = 'tin_dang';
    static $thanh_vien = 'thanh_vien';
    static $thanh_vien_site = 'thanh_vien_site';
}


class CommonNavigates
{

    public static function toPage($page, array $params = array())
    {
        $params = array_filter($params);
        if (count($params) > 0) {
            echo "<script>location.replace('admin.php?page=$page.php&" . http_build_query($params) . "');</script>";
        } else {
            echo "<script>location.replace('admin.php?page=$page.php');</script>";
        }
    }
    public static function toPage_VietTat(array $params = array())
    {
        self::toPage(NameTables::$viet_tat, $params);
    }

    public static function toPage_KieuNha(array $params = array())
    {
        self::toPage(NameTables::$kieu_nha, $params);
    }
    public static function toPage_Account(array $params = array())
    {
        self::toPage(NameTables::$account, $params);
    }

    public static function toPage_LogsLinkSite(array $params = array())
    {
        self::toPage(NameTables::$logs_link_site, $params);
    }
    public static function toPage_Schedule(array $params = array())
    {
        self::toPage(NameTables::$schedule, $params);
    }
    public static function toPage_MoTaThem(array $params = array())
    {
        self::toPage(NameTables::$mo_ta_them, $params);
    }
    public static function toPage_ThuocTinh(array $params = array())
    {
        self::toPage(NameTables::$thuoc_tinh, $params);
    }
    public static function toPage_DuAn(array $params = array())
    {
        self::toPage(NameTables::$du_an, $params);
    }
    public static function toPage_Config(array $params = array())
    {
        self::toPage(NameTables::$config, $params);
    }
    public static function toPage_TinDang(array $params = array())
    {
        self::toPage(NameTables::$tin_dang, $params);
    }

    public static function toPage_ThanhVien(array $params = array())
    {
        self::toPage(NameTables::$thanh_vien, $params);
    }
}
