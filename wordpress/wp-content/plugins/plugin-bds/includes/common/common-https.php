<?php

/**
 *  book common https.
 *
 * @package \Common
 */
class CommonHttps
{
    public static function get(string $url, array $params = array(), array $headers = array("Content-Type" => "application/json")): BDSResponse
    {
        $query_url = $url . '?' . http_build_query($params);
        $api_response = wp_remote_get($query_url, array(
            'headers' => $headers,
            'params' => $params,
            'timeout' => 300,

        ));

        return new BDSResponse(json_decode($api_response['body'], true));
    }
    public static function delete(string $url, string $id, array $headers = array("Content-Type" => "application/json")): BDSResponse
    {
        $api_response = wp_remote_request($url . $id, array(  // ?force=true to skip trash
            'method'    => 'DELETE',
            'headers' => $headers,
            'timeout' => 300,
        ));

        return new BDSResponse(json_decode($api_response['body'], true));
    }

    public static function post(string $url, array $body, array $headers = array('Content-Type' => 'application/json; charset=utf-8')): BDSResponse
    {
        try {
            $option = array(
                'headers' => $headers,
                'body' => json_encode($body),
                'timeout' => 500,
            );
            $api_response = wp_remote_post($url, $option);

            return new BDSResponse(json_decode($api_response['body'], true));
        } catch (Exception $e) {
            CommonFunctions::console_log($e);
        }
        return null;
    }

    public static function update(string $url, string $id, array $body, array $headers = array("Content-Type" => "application/json")): BDSResponse
    {

        $api_response = wp_remote_post($url . $id, array(
            'headers' => $headers,
            'body' => $body,
            'timeout' => 300,

        ));

        return new BDSResponse(json_decode($api_response['body'], true));
    }
}
