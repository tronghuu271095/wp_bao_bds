<?php

/**
 *  book common Methods.
 *
 * @package \Common
 */
class CommonMethods
{
    public static function remove_cache_linksite()
    {
        try {
            $query_url = Variables::$path_api . '/' . 'RemoveCacheLinkSite';
            $body = array();
            $res = CommonHttps::post($query_url,  $body);
            if ($res->isSuccess()) {
                CommonFunctions::console_log("Đã remove các link site");
            } else {
                CommonFunctions::console_log($res->getMesage());
            }
            return $res->isSuccess();
        } catch (Exception $e) {
            CommonFunctions::console_log($e);
        }
    }

    public static function clear_chrome()
    {
        try {
            $query_url = Variables::$path_api . '/' . 'ClearChrome';
            $body = array();
            $res = CommonHttps::post($query_url,  $body);
            if ($res->isSuccess()) {
                CommonFunctions::console_log("Đã làm mới chrome");
                static::remove_cache_linksite();
            } else {
                CommonFunctions::console_log($res->getMesage());
            }
            return $res->isSuccess();
        } catch (Exception $e) {
            CommonFunctions::console_log($e);
        }
    }

    /**
     * get_name_column_by_at.
     *
     * @param int $index vị trí column
     */
    public static function get_name_column_by_at(int $index): string
    {
        $list_name_column = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W");
        return $list_name_column[$index];
    }

    public static function is_not_null($input): bool
    {
        return CommonFunctions::string_is_not_null($input);
    }

    public static function get_rand_number(int $start, int $end, int $step = 1)
    {
        $new_array = range($start, $end, $step);
        return CommonFunctions::array_rand_item($new_array);
    }

    public static function get_rand_value($value, $step = 1)
    {
        if (!static::is_not_null($value)) return null;
        try {
            if (is_numeric($value)) return $value;
            $strGia = $value;
            if (strpos($strGia, '-') === false) {
                $data = CommonFunctions::string_to_array_number($value, ",");
                return CommonFunctions::array_rand_item($data);
            } else {
                $data = CommonFunctions::string_to_array_number($value, "-");
                $min = $data[0];
                $max = $data[1];
                if ($min > $max) {
                    $min = $data[1];
                    $max = $data[0];
                }
                $step = round($step, 1) * 10;
                $min = round($min, 1) * 10;
                $max = round($max, 1) * 10;
                return static::get_rand_number($min, $max, $step) / 10;
            }
        } catch (\Throwable $th) {
        }
        return 1;
    }

    public static function get_rand_bool(float $percent = 0.5)
    {
        $percent = ($percent < 0 || $percent >= 1) ? 0.5 : $percent;
        $pTrue = $percent * 10;
        $arr = array();
        for ($n = 1; $n <= 10; $n++) {
            array_push($arr, $n <= $pTrue);
        }
        return CommonFunctions::array_rand_item($arr);
    }

    public static function round_tong_gia(float $amount): int
    {

        $compacts = explode(" ", CommonFunctions::int_format_price($amount));
        $value = floatval(str_replace(",", ".", $compacts[0]));
        $value = round($value * 10) / 10;
        unset($compacts[0]);
        foreach ($compacts as $compact) {

            $compact = strtolower($compact);

            if ($compact == 'tỷ') {
                $value = $value * pow(10, 9);
            }
            if ($compact == 'triệu') {
                $value = $value * pow(10, 6);
            }
            if ($compact == 'nghìn') {
                $value = $value * pow(10, 3);
            }
        };
        return $value;
    }
    public static function get_obj_dia_chi(string $diaChi)
    {
        $obj = array();
        $arrDiaChi = array_map(
            function ($item) {
                return CommonFunctions::string_fix_text($item);
            },
            explode(",", $diaChi)
        );
        if (count($arrDiaChi) < 3 || count($arrDiaChi) > 4) return null;

        if (count($arrDiaChi) == 4) {
            $obj["SoNha"] = $arrDiaChi[0];
            unset($arrDiaChi[0]);
        }
        $obj["TenPhuongXa"] = $arrDiaChi[1];
        $obj["TenQuanHuyen"] = $arrDiaChi[2];
        $obj["TenTinhThanh"] = $arrDiaChi[3];
        return $obj;
    }

    public static function create_data(array $data_range, array $arr_max_length = [3000], $value = ''): array
    {
        $data = array();
        for ($i = 0; $i < Variables::$max_dequy; ++$i) {
            $max_length = CommonFunctions::array_rand_item($arr_max_length);
            $text = static::dequy_create_data($data_range, 0, $value, $max_length);
            $text = preg_replace('/%:%/', ":", $text);
            $text = trim($text);
            if (CommonFunctions::string_is_not_null($text) && !in_array($text, $data)) {
                array_push($data, $text);
            }
        }
        return $data;
    }
    private static function get_object_value(string $value): array
    {
        $data = array(
            'text' => '',
            'not_cell' => []
        );
        if (CommonFunctions::string_is_not_null($value)) {
            $arr = CommonFunctions::string_to_array($value, '::');
            $data["text"] = $arr[0];
            if (count($arr) > 1) {
                unset($arr[0]); // bỏ text đầu
                $data["not_cell"] = $arr;
            }
        }

        return $data;
    }
    private static function dequy_create_data(array $data_range, int $current_column, string $value, int $max_length): string
    {
        try {
            $end = count($data_range[0]) - 1;
            $current_column_next = $current_column + 1;
            $obj_value = static::get_object_value($value);
            if ($current_column > $end || (strlen($obj_value["text"]) >= $max_length)) return $obj_value["text"]; // ĐK 1
            $nameColumn = static::get_name_column_by_at($current_column);
            if (in_array($nameColumn, $obj_value["not_cell"])) {
                return static::dequy_create_data($data_range, $current_column_next, $value, $max_length);
            }
            $data = array();
            for ($i = 0; $i < count($data_range); ++$i) {
                $item = $data_range[$i][$current_column];
                if (!CommonFunctions::string_is_not_null($item)) break;
                array_push($data, $item);
            }

            $index_row = CommonFunctions::array_rand_index($data);
            $item = $data[$index_row];
            $name_cell = $nameColumn . ($index_row + 1);
            if (in_array($name_cell, $obj_value["not_cell"]) || in_array($nameColumn, $obj_value["not_cell"])) {
                return static::dequy_create_data($data_range, $current_column_next, $value, $max_length);
            }
            $obj_item =  static::get_object_value($item);
            $new_text = CommonFunctions::string_fix_text($obj_value["text"] . ' ' . $obj_item["text"]);
            $new_not_cell = array_unique(array_merge($obj_value["not_cell"], $obj_item["not_cell"]));
            if (strlen($obj_value["text"]) >= $max_length) return $obj_value["text"]; // ĐK 1
            $new_value = trim($new_text);
            if (count($new_not_cell) > 0) {
                $new_value .= ('::' . implode('::', $new_not_cell));
                $new_value = CommonFunctions::string_fix_text($new_value);
            }
            return static::dequy_create_data($data_range, $current_column_next, $new_value, $max_length);
        } catch (\Throwable $th) {
            CommonFunctions::console_log("error");
        }
        return "";
    }
    // chuyển đổi địa chỉ qua arr
    public static function to_arr_short_dia_chi(string $diaChi): array
    {

        $data = array();
        $arrDiaChi = array_map(function ($item) {
            return CommonFunctions::string_fix_text($item);
        }, explode(',', $diaChi));
        $lDiaChi = [$diaChi];
        $index = 0;
        foreach ($arrDiaChi as $item) {
            $index++;
            $dc = CommonFunctions::array_clone($arrDiaChi);
            if ($index > 0) unset($arrDiaChi[$index]); // lưu ý luôn phải có số nhà
            array_push($lDiaChi, implode(", ", $dc));
            if ($index == (count($arrDiaChi) - 1)) {
                $dc1 = CommonFunctions::array_clone($dc);
                unset($dc1[count($dc1) - 1]);
                array_push($lDiaChi, implode(", ", $dc1));
                array_push($lDiaChi, implode(", ", $dc1));
                array_push($lDiaChi, implode(", ", $dc1));
                $dc2 = CommonFunctions::array_clone($dc);
                unset($dc2[count($dc1) - 1]);
                array_push($lDiaChi, implode(", ", $dc2));
                array_push($lDiaChi, implode(", ", $dc2));
                array_push($lDiaChi, implode(", ", $dc2));
            }
        };

        $viet_tats = BDSVietTat::all();

        foreach ($lDiaChi as $dc) {
            foreach ($viet_tats as $viet_tat) {
                $key = $viet_tat->tu_goc;
                $tu_thay_the =  $viet_tat->rand_tu_thay_the();
                if (static::get_rand_bool(0.8) && strlen($tu_thay_the) > 1) {
                    $dc = str_replace($key, $tu_thay_the, $dc);
                }
            };
            array_push($data, preg_replace('/\. /', '.', $dc));
        }
        $data = CommonFunctions::array_sort_string($data, false);
        return $data;
    }
}
