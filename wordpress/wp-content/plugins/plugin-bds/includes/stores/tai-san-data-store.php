<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders

 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */
class TaiSanDataStore extends DataStore
{
    public $table_name = "du_an";
    public function create_table()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . Variables::$plugin_name . '_' . $this->table_name;
        $query = $wpdb->prepare('SHOW TABLES LIKE %s', $wpdb->esc_like($table_name));
        if (!$wpdb->get_var($query) == $table_name) {
            $sql = "CREATE TABLE `$table_name` (
				`id` int NOT NULL AUTO_INCREMENT,
                `id_du_an` int NOT NULL,
                `dia_chi` nvarchar DEFAULT NULL,                
                `gia` int DEFAULT NULL,
                `gia_tho_cu` int DEFAULT NULL,
                `gia_dat_khac` int DEFAULT NULL,
                `dien_tich` int DEFAULT NULL,
                `dien_tich_dat_tho_cu` int DEFAULT NULL,
                `dien_tich_dat_tho_khac` int DEFAULT NULL,
                `chieu_ngang` int DEFAULT NULL,
                `tieu_de` int DEFAULT NULL,
                `tien_ich` int DEFAULT NULL,
				PRIMARY KEY(Id)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
            $wpdb->query($sql);
        }
    }
}
