<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders

 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */
class TinDangDataStore extends DataStore
{
    public $table_name = "tin_dang";
    public function create_table()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . Variables::$plugin_name . '_' . $this->table_name;
        $query = $wpdb->prepare('SHOW TABLES LIKE %s', $wpdb->esc_like($table_name));
        if (!$wpdb->get_var($query) == $table_name) {
            $sql = "CREATE TABLE `$table_name` (
				`id` int NOT NULL AUTO_INCREMENT,
                `id_tin` int DEFAULT NULL,
                `is_vip` BIT DEFAULT 0,
                `id_du_an` int NOT NULL,
                `id_site` int NOT NULL,
                `id_kieu_nha` int DEFAULT NULL,
                `id_thanh_vien` int NOT NULL,
                `tieu_de` nvarchar(250) NOT NULL,
                `mo_ta` TEXT NOT NULL,
                `dia_chi` nvarchar(500) NOT NULL,
                `gia` int NOT NULL,
                `gia_dat_tho_cu` int DEFAULT NULL,
                `gia_dat_khac` int DEFAULT NULL,
                `don_vi_tinh` int DEFAULT 1,
                `dien_tich` int NOT NULL,
                `dien_tich_dat_tho_cu` int DEFAULT NULL,
                `dien_tich_dat_khac` int DEFAULT NULL,
                `chieu_ngang` int NOT NULL,
                `chieu_dai` int NOT NULL,
                `duong_vao` int DEFAULT NULL,
                `huong_nha` int DEFAULT NULL,
                `so_phong_ngu` int DEFAULT NULL,
                `so_toilet` int DEFAULT NULL,
                `so_tang` int DEFAULT NULL,
                `ket_cau` TEXT DEFAULT NULL,
                `thong_tin_phap_ly` TEXT DEFAULT NULL,
                `ghi_chu` TEXT DEFAULT NULL,
                `trang_thai` int DEFAULT -1,
                `ngay_tao` DATETIME NOT NULL DEFAULT NOW(),
                `ngay_dang` DATETIME DEFAULT NULL,
                `dang_sau` int DEFAULT NULL,
                `link_sua` TEXT DEFAULT NULL,
                `link_xem` TEXT DEFAULT NULL,

				PRIMARY KEY(id)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
            $wpdb->query($sql);
        }
    }
}
