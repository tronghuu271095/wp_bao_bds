<?php

/**
 * KieuNhaDataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders

 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */
class KieuNhaDataStore extends DataStore
{
    public $table_name = "kieu_nha";
    public function create_table()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . Variables::$plugin_name . '_' . $this->table_name;
        $query = $wpdb->prepare('SHOW TABLES LIKE %s', $wpdb->esc_like($table_name));
        if (!$wpdb->get_var($query) == $table_name) {
            $sql = "CREATE TABLE `$table_name` (
        		`id` int NOT NULL AUTO_INCREMENT,
        		`ten_kieu_nha` nvarchar(250) NOT NULL,
                `text_so_phong` varchar(250) DEFAULT NULL,
                `text_so_tang` varchar(250) DEFAULT NULL,
                `text_so_toilet` varchar(250) DEFAULT NULL,
                `text_ket_cau` TEXT DEFAULT NULL,
        		PRIMARY KEY(id)
        	) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
            $wpdb->query($sql);
        }
    }
}
