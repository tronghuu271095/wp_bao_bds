<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders

 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */
class DataStore
{
    protected $table_name = 'tbl';

    /**
     * Get the table used to store posts.
     *
     * @return string
     */
    public function get_table()
    {
        global $wpdb;
        return $wpdb->prefix . Variables::$plugin_name . '_' . $this->table_name;
    }
    /**
     * Get array of table used to store posts.
     *
     * @return array
     */
    public function get_col()
    {
        global $wpdb;
        $table = $this->get_table();
        return $wpdb->get_col("DESC {$table}", 0);
    }
    public function get()
    {
        global $wpdb;
        $table = $this->get_table();
        return $wpdb->get_results("SELECT * FROM $table");
    }

    public function find_one_by($property, $value)
    {
        global $wpdb;
        $table = $this->get_table();
        // return  $wpdb->get_results("SELECT * FROM $table Where  id = $value");
        // global $wpdb;
        // $table = $this->get_table(); 
        // // Escape the value
        $value = esc_sql($value);
        // // return $wpdb->get_results("SELECT * FROM $table Where id = $value");


        // // Get the table name
        return $wpdb->get_row("SELECT * FROM `{$table}` WHERE `{$property}` = '{$value}'", ARRAY_A);
    }
    public function getById($id)
    {
        global $wpdb;
        $table = $this->get_table();
        return $wpdb->get_results("SELECT * FROM $table Where id = $id");
    }
    public function insert($data, $format = null)
    {
        global $wpdb;
        $table = $this->get_table();
        return $wpdb->insert($table, $data, $format);
    }
    public function update($data, $where, $format = null, $where_format = null)
    {
        global $wpdb;
        $table = $this->get_table();
        return $wpdb->update($table, $data, $where, $format = null, $where_format = null);
    }
    public function delete($where)
    {
        global $wpdb;
        $table = $this->get_table();
        return $wpdb->delete($table, $where);
    }

    public function drop_table()
    {
        global $wpdb;
        $table = $this->get_table();
        return $wpdb->query("DROP TABLE IF EXISTS $table;");
        // delete_option("my_plugin_db_version");

    }
}
