<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */
if (!defined("ABSPATH")) {
    exit;
}
/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders
 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */
class DuAnDataStore extends DataStore
{
    public $table_name = "du_an";
    public function create_table()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . Variables::$plugin_name . '_' . $this->table_name;
        $query = $wpdb->prepare("SHOW TABLES LIKE %s", $wpdb->esc_like($table_name));
        if (!$wpdb->get_var($query) == $table_name) {
            $sql = "CREATE TABLE `$table_name` (
				`id` int NOT NULL AUTO_INCREMENT,
                `ma_du_an` varchar(220) NOT NULL,
                `ten_du_an` nvarchar(250) DEFAULT NULL,
                `ten_duong` nvarchar(250) DEFAULT NULL,
                `url_drive` varchar(220) DEFAULT NULL,
                `loai` int NOT NULL,
                `text_gia` varchar(220) DEFAULT NULL,
                `text_gia_dat_tho_cu` varchar(220) DEFAULT NULL,
                `text_gia_dat_khac` varchar(220) DEFAULT NULL,
                `text_dien_tich` varchar(220) DEFAULT NULL,
                `text_dien_tich_dat_tho_cu` varchar(220) DEFAULT NULL,
                `text_dien_tich_dat_khac` varchar(220) DEFAULT NULL,
                `text_chieu_ngang` varchar(220) DEFAULT NULL,
                `text_tieu_de` TEXT DEFAULT NULL,
                `text_tien_ich` TEXT DEFAULT NULL,
                `text_vi_tri` TEXT DEFAULT NULL,
                `link_checks` TEXT DEFAULT NULL,
                `active` BIT DEFAULT 1,

				PRIMARY KEY(Id)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
            $wpdb->query($sql);
        }
    }
}
