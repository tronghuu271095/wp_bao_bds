<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders

 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */
class SiteDataStore extends DataStore
{
    public $table_name = "site";
    public function create_table()
    {

        global $wpdb;
        $table_name = $wpdb->prefix . Variables::$plugin_name . '_' . $this->table_name;
        $query = $wpdb->prepare('SHOW TABLES LIKE %s', $wpdb->esc_like($table_name));
        if (!$wpdb->get_var($query) == $table_name) {
            $sql = "CREATE TABLE `$table_name` (
				`id` int NOT NULL AUTO_INCREMENT,
				`ten_site` nvarchar(220) NOT NULL,
                `url_home` varchar(220) DEFAULT NULL,
                `max_image` int DEFAULT 1,
				PRIMARY KEY(id)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
            $wpdb->query($sql);
            $dataInit = array(
                array(
                    "ten_site" => "batdongsan",
                    "url_home" => "https://batdongsan.com.vn",
                ),
                array(
                    "ten_site" => "dothi",
                    "url_home" => "https://dothi.net",
                ),
                array(
                    "ten_site" => "bannhasg",
                    "url_home" => "http://bannhasg.com",
                ),
                array(
                    "ten_site" => "alonhadat",
                    "url_home" => "https://alonhadat.com.vn/",
                )
            );
            foreach ($dataInit as $element) {
                $this->insert($element);
            }
        }
    }
}
