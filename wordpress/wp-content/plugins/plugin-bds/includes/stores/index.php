<?php
include 'data-store.php';
include 'viet-tat-data-store.php';
include 'kieunha-data-store.php';
include 'logs-linksite-data-store.php';
include 'motathem-data-store.php';
include 'thuoctinh-data-store.php';
include 'duan-data-store.php';
include 'tindang-data-store.php';
include 'thanhvien-data-store.php';
include 'thanhvien-site-data-store.php';

class Stores
{
    static function init()
    {
        (new VietTatDataStore())->create_table();
        (new KieuNhaDataStore())->create_table();
        (new LogsLinkSiteDataStore())->create_table();
        (new MoTaThemDataStore())->create_table();
        (new ThuocTinhDataStore())->create_table();
        (new DuAnDataStore())->create_table();
        (new TinDangDataStore())->create_table();
        (new ThanhVienDataStore())->create_table();
        (new ThanhVienSiteDataStore())->create_table();
    }
    static function destroy()
    {
        (new VietTatDataStore())->drop_table();
        (new KieuNhaDataStore())->drop_table();
        (new LogsLinkSiteDataStore())->drop_table();
        (new MoTaThemDataStore())->drop_table();
        (new ThuocTinhDataStore())->drop_table();
        (new DuAnDataStore())->drop_table();
        (new TinDangDataStore())->drop_table();
        (new ThanhVienDataStore())->create_table();
        (new ThanhVienSiteDataStore())->create_table();
    }
}
