<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 *  Book Data Store.
 * https://onlinewebtutorblog.com/wordpress-crud-tutorial-with-wpdb-object/
 * Data Types Placeholders

 *  %d – Integer Value
 *  %s – String Value
 *  %f – Float Value etc.
 * @version  1.0.0
 */
class LogsLinkSiteDataStore extends DataStore
{
    public $table_name = "logs_link_site";
    public function create_table()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . Variables::$plugin_name . '_' . $this->table_name;
        $query = $wpdb->prepare('SHOW TABLES LIKE %s', $wpdb->esc_like($table_name));
        if (!$wpdb->get_var($query) == $table_name) {
            $sql = "CREATE TABLE `$table_name` (
				`id` int NOT NULL AUTO_INCREMENT,
                `id_du_an` int NOT NULL,
                `link` nvarchar(250) DEFAULT NULL,
                `ten_nguoi_dang` nvarchar(250) DEFAULT NULL,
                `text_gia` nvarchar(250) DEFAULT NULL,
                `text_dien_tich` nvarchar(250) DEFAULT NULL,
                `text_dien_thoai` nvarchar(250) DEFAULT NULL,
                `ngay_tao` DATETIME NOT NULL DEFAULT NOW(),
				PRIMARY KEY(id)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
            $wpdb->query($sql);
        }
    }
}
