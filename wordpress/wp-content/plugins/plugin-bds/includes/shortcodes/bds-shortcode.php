<?php

/**
 * Plugin Name: TBare Wodpress Plugin demo
 * Plugin URI: https://www.tbare.com
 * Description: Display content using a shortcode to insert in a page or post
 * Version: 0.1
 * Text Domain: tbare-wordpress-plugin-demo
 * Author: Tim Bare
 * Author URI: https://www.tbare.com
 */

function fnc_ui_zocial($atts, $content = null)
{
    // set up default parameters
    extract(shortcode_atts(array(
        'ma' => 'facebook'
    ), $atts));
    $bds = BDS_Model::getBDSByCode($ma);
    $content = $content ?? $bds->name;
    return '<a href="' . $bds->value . '" target="blank" class="doti-button">' . $content . '</a>';
}

add_shortcode('ui-bds', 'fnc_ui_zocial');
