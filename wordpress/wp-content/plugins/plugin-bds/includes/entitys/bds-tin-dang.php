
<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}
class BDSTinDang extends AbstractEntity
{
    public $id;
    public $id_tin;
    public $is_vip = 0;
    public $id_du_an;
    public $id_site = 1;
    public $id_kieu_nha;
    public $id_thanh_vien;
    public $tieu_de;
    public $mo_ta;
    public $dia_chi;
    public $gia;
    public $gia_dat_tho_cu;
    public $gia_dat_khac;
    public $don_vi_tinh;
    public $dien_tich;
    public $dien_tich_dat_tho_cu;
    public $dien_tich_dat_khac;
    public $chieu_ngang;
    public $chieu_dai;
    public $huong_nha;
    public $so_phong_ngu;
    public $so_toilet;
    public $so_tang;
    public $ket_cau;
    public $thong_tin_phap_ly;
    public $ghi_chu;
    public $trang_thai;
    public $ngay_tao;
    public $ngay_dang;
    public $link_xem;
    public $link_sua;
    public $dang_sau;
    public $duong_vao;

    public function save()
    {
        if (!$this->ngay_tao) {
            $this->ngay_tao = new DateTime('NOW', new DateTimeZone('+0700'));
        }

        $this->is_vip = $this->is_vip == 1 || $this->is_vip == true;
        parent::save();
    }

    public static function store()
    {
        return new TinDangDataStore();
    }
    public function du_an(): BDSDuAn
    {
        return BDSDuAn::find_one($this->id_du_an);
    }

    public function duplicate(): BDSTinDang
    {
        $item_duplicate = $this;
        if ($this->id > 0) {
            $item_duplicate->id = null;
            $item_duplicate->id_tin = null;
            $item_duplicate->ngay_tao = null;
            $item_duplicate->trang_thai = -1;
            $item_duplicate->ghi_chu = null;
            $now = new DateTime('NOW', new DateTimeZone('+0700'));
            if ($item_duplicate->ngay_dang == null ||  ($item_duplicate->ngay_dang <= $now)) {
                $item_duplicate->ngay_dang = null;
            }
            $item_duplicate->save();
            return $item_duplicate;
        }
        return null;
    }

    public function kieu_nha(): BDSKieuNha
    {
        return BDSKieuNha::find_one($this->id_kieu_nha);
    }

    public function name_site(): string
    {
        return Variables::$site[$this->id_site];
    }

    public function thanh_vien(): BDSThanhVien
    {
        return BDSThanhVien::find_one($this->id_thanh_vien);
    }

    public function account(): BDSThanhVienSite
    {
        return $this->thanh_vien()->account($this->id_site);
    }

    public function text_trang_thai(): string
    {
        switch ($this->trang_thai) {
            case -1:
                return "Mới tạo";
            case 1:
                return "Chờ duyệt";
            case 2:
                return "Đã duyệt";
            case 3:
                return "Từ chối duyệt";
        }
        return "Không xác định";
    }
    public function loai(): int
    {
        try {
            return $this->du_an()->loai;
        } catch (\Throwable $th) {
            //throw $th;
        }
        return 1;
    }
    public function cap_nhat_trang_thai()
    {
        try {
            if (CommonMethods::is_not_null($this->id)) {
                $account = $this->account();
                $body = array(
                    "TenDangNhap" => $account->ten_dang_nhap,
                    "MatKhau" => $account->mat_khau,
                    "Id" => $this->id_tin,
                    "IdSite" => $this->id_site
                );
                $query_url = Variables::$path_api . '/GetStatusLink';
                $res = CommonHttps::post($query_url,  $body);
                if ($res->isSuccess()) {
                    $data = $res->firstData();
                    if (CommonMethods::is_not_null($data["link-xem"])) {
                        $this->trang_thai = 2;
                        $this->link_xem = $data["link-xem"];
                        $this->ngay_dang = new DateTime('NOW', new DateTimeZone('+0700'));
                    } else {
                        if (CommonMethods::is_not_null($data["ghi-chu"])) {
                            $this->trang_thai = 3;
                        }
                    }
                    $this->ghi_chu = $data["ghi-chu"];
                } else {
                    $this->ghi_chu = $res->getMesage();
                }
                $this->save();
            }
        } catch (Exception $e) {
            CommonFunctions::console_log($e);
        }
    }

    public function post()
    {
        $body = $this->to_json_tin_dang();

        try {
            $query_url = Variables::$path_api;
            $res = CommonHttps::post($query_url,  $body);
            if ($res->isSuccess()) {
                $data = $res->firstData();
                if ($data["id"] > 0) {
                    if ($this->id_tin > 0) {
                        $this->id = null; // tạo thêm record với các tin đã đăng
                    }
                    $this->trang_thai = 1;
                    $this->id_tin = $data["id"];
                    $this->link_xem = $data["link-xem"];
                    $this->link_sua = $data["link-sua"];
                    $this->ghi_chu = "Tin đã đăng";

                    if (CommonFunctions::string_is_not_null($this->link_xem)) {
                        $this->trang_thai = 2; // đã duyệt
                    }
                } else {
                    $this->ghi_chu = "Đăng tin thất bại";
                }
            } else {
                $this->ghi_chu = $res->getMesage();
            }
            $this->save();
        } catch (Exception $e) {
            CommonFunctions::console_log($e);
        }
        return $this;
    }
    public function to_json_tin_dang()
    {
        $du_an = $this->du_an();
        $data = array(
            "IdSite" =>  intval($this->id_site),
            "IsVip" => !!$this->is_vip,
            "Ma" =>  $du_an->ma_du_an,
            "TenDuAn" => $du_an->ten_du_an,
            "TenLoai" => $du_an->ten_loai(),
            "TieuDe" => $this->tieu_de,
            "MoTa" => $this->mo_ta,
            "DiaChi" => $this->dia_chi,
            "Gia" =>  intval($this->gia),
            "DonViTinh" => intval($this->don_vi_tinh),
            "DienTich" => intval($this->dien_tich),
            "Ngang" => floatval($this->chieu_ngang),
            "Dai" => floatval($this->chieu_dai),
            "ThongTinPhapLy" => $this->thong_tin_phap_ly,
        );

        if ($du_an->loai > 1) {
            $kieu_nha = $this->kieu_nha();
            $data["KieuNha"] =  $kieu_nha->ten_kieu_nha;
            $data["DuongVao"]  = intval($this->duong_vao);
            $data["HuongNha"] = intval($this->huong_nha);
            $data["SoPhongNgu"] = intval($this->so_phong_ngu);
            $data["SoToilet"]  = intval($this->so_toilet);
            $data["SoTang"]  = intval($this->so_tang);
        }
        $thanh_vien = $this->thanh_vien();
        $account = $this->account();
        $data["TenDangNhap"] =  $account->ten_dang_nhap;
        $data["MatKhau"] =  $account->mat_khau;
        $data["DiaChiLienHe"] =  $thanh_vien->dia_chi;
        $data["DienThoaiLienHe"] =  $thanh_vien->dien_thoai;
        $data["EmailLienHe"] =  $thanh_vien->email;
        $data["TenLienHe"] =  $thanh_vien->ho_ten;
        $data_dia_chi = CommonMethods::get_obj_dia_chi($this->dia_chi);
        $data["TenTinhThanh"] =  $data_dia_chi["TenTinhThanh"];
        $data["TenQuanHuyen"] =  $data_dia_chi["TenQuanHuyen"];
        $data["TenPhuongXa"] =  $data_dia_chi["TenPhuongXa"];
        $data["SoNha"] =  $data_dia_chi["SoNha"];
        $data["ThoiGianDang"] = 7; //giá trị mặc định

        $now = new DateTime('NOW', new DateTimeZone('+0700'));
        $dang_sau = $this->dang_sau ?? 0;
        $tu_ngay =  $now;
        if ($dang_sau > 0) {
            $tu_ngay =  $now->add(new DateInterval("P" . $dang_sau . "D"));
        }
        $data["TuNgay"] = $tu_ngay->format('c');
        $data["DenNgay"] = $tu_ngay->add(new DateInterval('P7D'))->format('c');
        return $data;
    }
    // // tạm giữ lại build_tieu_de trong file này
    public static function build_tieu_de($dia_chi, $root_tieu_des): array
    {
        $tieu_des = array();
        $arrDiaChi = CommonMethods::to_arr_short_dia_chi($dia_chi);
        $arrMaxLength = array_map(function ($dc) {
            return Variables::$max_length_tieu_de - strlen($dc) - 2;
        }, $arrDiaChi);
        $tieu_des = BDSThuocTinh::get_data_tieu_de($arrMaxLength);
        $tieu_des = array_unique(array_merge($tieu_des, $root_tieu_des));
        // $tieu_des = array_filter($tieu_des);
        $tieu_des = array_map(function ($text) {
            if (strpos($text, "%diachi%") === false) {
                $text .= " %diachi%";
            }
            if ($text != null && strpos($text, "bán") === false && strpos($text, "chuyển nhượng") === false && strpos($text, "sang") === false) {
                $text =  preg_replace('/(.+)?%diachi%(.+)?/', '$1' . (CommonMethods::get_rand_bool() ? " cần bán " : " muốn bán ") . '%diachi%$2', $text);
            }
            $text =  preg_replace('/ +/', ' ', $text);
            return $text;
        }, $tieu_des);
        return $tieu_des;
    }
    // Build dữ liệu theo mã dự án
    public static function build_data_du_an(array $params, bool $is_create = true)
    {
        try {
            $id_site = $params["id_site"] ?? -1;
            $id_du_an = $params["id_du_an"] ?? 1;
            $max_build = $params["build_num"] ?? Variables::$max_build;
            $max_build = ($max_build > 0 && $max_build < Variables::$max_build) ? $max_build : Variables::$max_build;
            $duAn = BDSDuAn::find_one($id_du_an);
            $dia_chis =  $duAn->dia_chis();
            $root_tieu_des =  $duAn->tieu_des();
            $root_thanhviens = BDSThanhVien::query()->where('active', 1)->find();
            $data = array();
            foreach ($dia_chis as $dia_chi) {
                $root_tieu_des = static::build_tieu_de($dia_chi, $root_tieu_des);
                $tieu_des = CommonFunctions::array_clone($root_tieu_des);
                $thanhviens = CommonFunctions::array_clone($root_thanhviens);
                for ($i = 1; $i <= $max_build; ++$i) {
                    $item = new BDSTinDang();
                    $item->dia_chi = $dia_chi;
                    $item->id_du_an = $duAn->id;
                    $item->is_vip = false;
                    $item->id_site = $id_site;
                    $item->trang_thai = -1;
                    //Xử lý tiêu đề
                    if (count($tieu_des) == 0) {
                        $tieu_des = CommonFunctions::array_clone($root_tieu_des);
                    }
                    $indexTieuDe = CommonFunctions::array_rand_index($tieu_des);
                    $tieuDe = $tieu_des[$indexTieuDe];
                    if (count($thanhviens) == 0) {
                        $thanhviens = CommonFunctions::array_clone($root_thanhviens);
                    }
                    $indexThanhVien = CommonFunctions::array_rand_index($thanhviens);
                    $thanh_vien = $thanhviens[$indexThanhVien];
                    $item->id_thanh_vien = $thanh_vien->id;
                    $item->tieu_de = $tieuDe;
                    unset($tieu_des[$indexTieuDe]);
                    //=== Diện tích
                    $data_dien_tich = $duAn->build_dien_tich();
                    $item->dien_tich = $data_dien_tich['dien_tich'];
                    $item->chieu_ngang = $data_dien_tich['chieu_ngang'];
                    $item->chieu_dai = $data_dien_tich['chieu_dai'];
                    $item->dien_tich_dat_tho_cu = $data_dien_tich['dien_tich_dat_tho_cu'];
                    $item->dien_tich_dat_khac  = $data_dien_tich['dien_tich_dat_khac'];
                    // //giá
                    $don_vi_tinh = CommonFunctions::array_rand_item([1, 2]);
                    $item->don_vi_tinh = $don_vi_tinh;
                    $data_gia = $duAn->build_gia();
                    $item->gia = $data_gia['gia'];
                    $item->gia_dat_tho_cu = $data_gia['gia_dat_tho_cu'];
                    $item->gia_dat_khac = $data_gia['gia_dat_khac'];
                    $huong_nha = null;
                    $objkieu_nha = null;
                    if ($duAn->loai > 1) {
                        $huong_nha = CommonFunctions::array_rand_index(Variables::$huong_nha);
                        $objkieu_nha = BDSKieuNha::get_rand();
                        $data_rand_kieu_nha = $objkieu_nha->rand_data();
                        $item->huong_nha = $huong_nha;
                        $item->id_kieu_nha = $data_rand_kieu_nha["id"];
                        $item->so_phong_ngu = $data_rand_kieu_nha["so_phong_ngu"];
                        $item->so_toilet = $data_rand_kieu_nha["so_toilet"];
                        $item->so_tang =  $data_rand_kieu_nha["so_tang"];
                        $item->ket_cau =  $data_rand_kieu_nha["ket_cau"];
                    }
                    $item->format_tieu_de();
                    $item->format_mo_ta();
                    array_push($data, $item);
                    if ($id_site == -1) {
                        foreach (array_keys(Variables::$site) as $site) {
                            $item->id = null;
                            $item->id_site = $site;
                            $item->save();
                        }
                    } else {
                        $item->save();
                    }
                };
            }
        } catch (Exception $ex) {
            CommonFunctions::console_log($ex);
        }

        return $data;
    }

    public function replace_text($textRoot)
    {
        try {
            if (strpos($textRoot, "%dientich%") !== false && $this->dien_tich > 0) {
                $txtRep = CommonFunctions::int_format_number($this->dien_tich) . ' m2';
                $textRoot = preg_replace('/%dientich%/', $txtRep, $textRoot);
            }
            if (strpos($textRoot, "%dientichdatthocu%") !== false && $this->dien_tich_dat_tho_cu > 0) {
                $txtRep = CommonFunctions::int_format_number($this->dien_tich_dat_tho_cu) . ' m2';
                $textRoot = preg_replace('/%dientichdatthocu%/', $txtRep, $textRoot);
            }
            if (strpos($textRoot, "%dientichdatkhac%") !== false && $this->dien_tich_dat_khac > 0) {
                $txtRep = CommonFunctions::int_format_number($this->dien_tich_dat_khac) . ' m2';
                $textRoot = preg_replace('/%dientichdatkhac%/', $txtRep, $textRoot);
            }
            if (strpos($textRoot, "%chieungang%") !== false && $this->chieu_ngang > 0) {
                $txtRep = CommonFunctions::int_format_number($this->chieu_ngang) . ' m';
                $textRoot = preg_replace('/%chieungang%/', $txtRep, $textRoot);
            }
            if (strpos($textRoot, "%chieudai%") !== false && $this->chieu_dai > 0) {
                $txtRep = CommonFunctions::int_format_number($this->chieu_dai) . ' m';
                $textRoot = preg_replace('/%chieudai%/', $txtRep, $textRoot);
            }
            if (strpos($textRoot, "%gia_m2%") !== false && $this->gia > 0) {
                $txtRep = CommonFunctions::int_format_price($this->gia) . '/m2';
                $textRoot = preg_replace('/%gia_m2%/', $txtRep, $textRoot);
            }

            if (strpos($textRoot, "%giadatthocu%") !== false && $this->gia_dat_tho_cu > 0) {
                $txtRep = CommonFunctions::int_format_price($this->gia_dat_tho_cu) . '/m2';
                $textRoot = preg_replace('/%giadatthocu%/', $txtRep, $textRoot);
            }

            if (strpos($textRoot, "%giadatkhac%") !== false && $this->gia_dat_khac > 0) {
                $txtRep = CommonFunctions::int_format_price($this->gia_dat_khac) . '/m2';
                $textRoot = preg_replace('/%giadatkhac%/', $txtRep, $textRoot);
            }

            if (strpos($textRoot, "%gia_tong%") !== false && $this->gia > 0) {
                $giaTong = CommonMethods::round_tong_gia($this->gia  * $this->dien_tich);
                $txtRep = CommonFunctions::int_format_price($giaTong);
                $textRoot = preg_replace('/%gia_tong%/', $txtRep, $textRoot);
            }
            if ($this->loai() > 1) {
                if (strpos($textRoot, "%huongnha%") !== false && $this->huong_nha > 0) {
                    $txtRep = Variables::$huong_nha[$this->huong_nha];
                    $textRoot = preg_replace('/%huongnha%/', $txtRep, $textRoot);
                }
                if (strpos($textRoot, "%sophongngu%") !== false && $this->so_phong_ngu > 0) {
                    $txtRep = $this->so_phong_ngu;
                    $textRoot = preg_replace('/%sophongngu%/', $txtRep, $textRoot);
                }
                if (strpos($textRoot, "%sotoilet%") !== false && $this->so_toilet > 0) {
                    $txtRep = $this->so_toilet;
                    $textRoot = preg_replace('/%sotoilet%/', $txtRep, $textRoot);
                }
            }
        } catch (\Throwable $th) {
            CommonFunctions::console_log("error");
        }

        return $textRoot;
    }

    public function format_tieu_de()
    {
        $tieuDe = $this->tieu_de;
        $arr_dia_chi =  CommonMethods::to_arr_short_dia_chi($this->dia_chi);
        $tieuDe = $this->replace_text($tieuDe);
        $txtroot = $tieuDe;
        $max_length_tieu_de = Variables::$max_length_tieu_de -  strlen($tieuDe) + strlen("%diachi%");
        $ldia_chi = array_filter(
            $arr_dia_chi,
            fn ($dc) => $max_length_tieu_de >= strlen($dc)
        );
        if (count($ldia_chi) == 0) {
            $arr_dia_chi = CommonMethods::to_arr_short_dia_chi($this->dia_chi);

            $ldia_chi = array_filter(
                $arr_dia_chi,
                fn ($dc) => $max_length_tieu_de >= strlen($dc)
            );
        }
        $dc = "";
        if (count($ldia_chi) > 0) {
            $dc = CommonFunctions::array_rand_item($ldia_chi);
            $tieuDe = preg_replace('/(.+)?(%diachi%)(.+)?/', ('$1' . $dc . '$3'), $tieuDe);
        }

        $tieuDe = str_replace("%diachi%", "", $tieuDe);
        $this->tieu_de = substr(CommonFunctions::string_fix_text($tieuDe), 0, Variables::$max_length_tieu_de);

        return $this;
    }

    public function format_text_dien_tich()
    {
        $dienTich =  CommonFunctions::int_format_number($this->dien_tich) . ' m2';
        $chieuchieu_ngang = CommonFunctions::int_format_number($this->chieu_ngang) . ' m2';
        $chieuchieu_dai = CommonFunctions::int_format_number($this->chieu_dai) . ' m2';
        $text = CommonFunctions::array_rand_item(Variables::$text_dien_tichs);
        $text = str_replace("%dientich%", $dienTich, $text);
        $text = str_replace("%chieungang%", $chieuchieu_ngang, $text);
        $text = str_replace("%chieudai%", $chieuchieu_dai, $text);
        if (CommonMethods::get_rand_bool(0.7) && $this->dien_tich_dat_tho_cu > 0) {
            $textphu = CommonFunctions::array_rand_item(Variables::$text_dien_tich_phus);
            if ($this->dien_tich_dat_tho_cu > 0) {
                $dienTichdat_tho_cu = CommonFunctions::int_format_number($this->dien_tich_dat_tho_cu) . ' m2';
                $textphu = str_replace("%dientichdatthocu%", $$dienTichdat_tho_cu, $textphu);
            }
            if ($this->dien_tich_dat_khac > 0) {
                $dienTichdat_khac = CommonFunctions::int_format_number($this->dien_tich_dat_khac) . ' m2';
                $textphu = str_replace("%dientichdatkhac%", $$dienTichdat_khac, $textphu);
            }
            $textphu = str_replace("%dientichdatthocu%", "", $textphu);
            $textphu = str_replace("%dientichdatkhac%", "", $textphu);
            if (CommonFunctions::string_is_not_null($textphu)) $text .= (', ' . $textphu . '.');
        }
        return CommonFunctions::string_fix_text($text);
    }
    public function format_text_gia()
    {
        if ($this->don_vi_tinh == 2) {
            return CommonFunctions::int_format_price(CommonMethods::round_tong_gia($this->gia * $this->dien_tich));
        } else {
            return CommonFunctions::int_format_number($this->gia) . ' đồng/m2';
        }
    }
    public function format_mo_ta()
    {
        $vi_tri = CommonFunctions::string_fix_text(CommonFunctions::array_rand_item(BDSThuocTinh::get_data_vi_tri()));

        $dien_tich = CommonFunctions::string_fix_text(CommonFunctions::array_rand_item(BDSThuocTinh::get_data_dien_tich()));

        $gia = CommonFunctions::string_fix_text(CommonFunctions::array_rand_item(BDSThuocTinh::get_data_gia()));

        $hien_trang = CommonFunctions::string_fix_text(CommonFunctions::array_rand_item(BDSThuocTinh::get_data_hien_trang()));

        $thong_tin_phap_ly = CommonFunctions::string_fix_text(CommonFunctions::array_rand_item(BDSThuocTinh::get_data_thong_tin_phap_ly()));

        $arrMota = array();
        $charStart = CommonFunctions::array_rand_item(Variables::$char_starts);
        array_push($arrMota, $vi_tri);

        $textDT = $this->format_text_dien_tich();
        $textDT = preg_replace('/%dientich%(.+)?/', $dien_tich . (CommonMethods::get_rand_bool(0.7) ? '' : '$1'), $textDT);
        array_push($arrMota, $textDT);

        $textgia = $this->format_text_gia();
        if ($this->loai() == 1 && $this->gia_dat_tho_cu > 0) {
            $txtgia_dat_tho_cu = CommonFunctions::array_rand_item(Variables::$text_gia_dat_tho_cus);
            $txtgia_dat_tho_cu = str_replace("%gia_datthocu%", $txtgia_dat_tho_cu, CommonFunctions::int_format_price($this->gia_dat_tho_cu) . "/m2");
            if (strlen($txtgia_dat_tho_cu) > 1) $textgia .= ', ' . $txtgia_dat_tho_cu;
        };
        $textgia = preg_replace('/%gia%(.+)?/', $gia . (CommonMethods::get_rand_bool(0.7) ? '' : '$1'), $textgia);
        array_push($arrMota, $textgia);
        $this->thong_tin_phap_ly = $thong_tin_phap_ly;
        array_push($arrMota, $thong_tin_phap_ly);
        array_push($arrMota, "Hiện trạng: " . $hien_trang);
        // xử lý cho loại nhà mặt phố
        if ($this->loai() > 1) {
            $text_huong_nha = CommonFunctions::array_rand_item(Variables::$text_huong_nhas);
            $text_huong_nha = str_replace("%huongnha%", Variables::$huong_nha[$this->huong_nha], $text_huong_nha);
            array_push($arrMota, $text_huong_nha);
            $text_ket_cau = $this->ket_cau;
            $text_ket_cau = $this->replace_text($text_ket_cau);
            array_push($arrMota, $text_ket_cau);
        }
        $arrMota = CommonMethods::get_rand_bool(0.3) ? array($charStart . implode(". ", $arrMota)) : array_map(fn ($mt) => $charStart . $mt, $arrMota);
        //Xử lý add tienich
        $arrTienIch = array();
        $duAn = BDSDuAn::find_one($this->id_du_an);
        $tien_ichs = $duAn->tien_ichs();
        //danh sách group_tien_ichs
        $keys_tien_ichs = array_keys($tien_ichs);
        //đk lấy danh sách tiện ích từ 1-5 tiện ích
        $num_rand = CommonMethods::get_rand_value("1-5", 1);
        $keys_tien_ichs =  CommonFunctions::array_new_rand($keys_tien_ichs, $num_rand);
        foreach ($keys_tien_ichs as $group) {
            $txt = CommonFunctions::array_rand_item($tien_ichs[$group]);
            array_push($arrTienIch, $txt);
        }

        $arrTienIch = CommonMethods::get_rand_bool(0.3) ? [$charStart . implode(". ", $arrTienIch)] : array_map(fn ($mt) => $charStart . $mt, $arrTienIch);
        if (count($arrTienIch)) {
            $arrMota = array_merge($arrMota, $arrTienIch);
        }
        shuffle($arrMota);
        $this->mo_ta =  implode("\n", $arrMota);

        //Xử lý text khoảng cách
        while (strpos($this->mo_ta, "%khoangcachm%") !== false) {
            $this->mo_ta = str_replace("%khoangcachm%", CommonMethods::get_rand_value("100-900", 100) . ' m', $this->mo_ta);
        }
        while (strpos($this->mo_ta, "%khoangcachkm%") !== false) {
            $this->mo_ta = str_replace("%khoangcachkm%", CommonMethods::get_rand_value("1-5", 1) . 'km', $this->mo_ta);
        }

        //Xử lý add MoTaThem
        $this->add_mo_ta_them();
        return $this;
    }
    // //add motathem
    public function add_mo_ta_them()
    {
        $moTaThems = BDSMoTaThem::all();
        $moTaThems = array_filter($moTaThems, fn ($item) => ($item->is_ban_dat == null || $item->is_ban_dat == ($item->loai() == 1)));
        $moTaThems = CommonFunctions::array_to_json($moTaThems);
        $group_mo_ta_thems = CommonFunctions::array_group_by($moTaThems, "nhom");
        $mo_ta_phu = array();
        $mo_ta_bat_buoc = array();
        foreach (array_keys($group_mo_ta_thems) as $group) {
            if (in_array($group, Variables::$group_tien_ich_required) != null) {
                $mo_ta_bat_buoc[$group] = $group_mo_ta_thems[$group];
            } else {
                $mo_ta_phu[$group] = $group_mo_ta_thems[$group];
            }
        };

        $keys_mo_ta_phu = array_keys($mo_ta_phu);
        $num_rand = CommonMethods::get_rand_value("1-3", 1);
        $keys_mo_ta_phu = CommonFunctions::array_new_rand($keys_mo_ta_phu, $num_rand);
        //đã trộn();
        foreach ($keys_mo_ta_phu as $group) {
            $mota = CommonFunctions::array_rand_item($mo_ta_phu[$group]);
            if (CommonFunctions::string_is_not_null($mota["noi_dung"])) {
                if ($mota["is_top"]) {
                    $this->mo_ta = $mota["noi_dung"] . "\n" . $this->mo_ta;
                } else {
                    $this->mo_ta .= "\n" . $mota["noi_dung"];
                }
            }
        }
        foreach (array_keys($mo_ta_bat_buoc) as $group) {
            $mota = CommonFunctions::array_rand_item($mo_ta_bat_buoc[$group]);
            if (CommonFunctions::string_is_not_null($mota["noi_dung"])) {
                if ($mota["is_top"]) {
                    $this->mo_ta = $mota["noi_dung"] . "\n" . $this->mo_ta;
                } else {
                    $this->mo_ta .= "\n" . $mota["noi_dung"];
                }
            }
        };
        return $this;
    }
}

function bds_del_tin_dang()
{
    $ids = $_POST['ids'];
    $list = BDSTinDang::query()->where_in("id", $ids)->find();
    foreach ($list as $item) {
        $item->delete();
    };
    echo json_encode($ids);
    exit();
}
add_action('wp_ajax_nopriv_bds_del_tin_dang', 'bds_del_tin_dang');
add_action('wp_ajax_bds_del_tin_dang', 'bds_del_tin_dang');

function bds_up_news()
{
    $ids = $_POST['ids'];
    $list = BDSTinDang::query()->where_in("id", $ids)->find();
    foreach ($list as $item) {
        $item->post();
    };
    echo json_encode($ids);
    exit();
}
add_action('wp_ajax_nopriv_bds_up_news', 'bds_up_news');
add_action('wp_ajax_bds_up_news', 'bds_up_news');

function bds_duplicate_news()
{
    $ids = $_POST['ids'];
    $list = BDSTinDang::query()->where_in("id", $ids)->find();
    foreach ($list as $item) {
        $item->duplicate();
    };
    echo json_encode($ids);
    exit();
}
add_action('wp_ajax_nopriv_bds_duplicate_news', 'bds_duplicate_news');
add_action('wp_ajax_bds_duplicate_news', 'bds_duplicate_news');
