<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}
class BDSDuAn extends AbstractEntity
{
    public $id;
    public $ma_du_an;
    public $ten_du_an;
    public $ten_duong;
    public $url_drive;
    public $loai;
    public $text_gia;
    public $text_gia_dat_tho_cu;
    public $text_gia_dat_khac;
    public $text_dien_tich;
    public $text_dien_tich_dat_tho_cu;
    public $text_dien_tich_dat_khac;
    public $text_chieu_ngang;
    public $text_tieu_de;
    public $text_tien_ich;
    public $text_vi_tri;
    public $link_checks;
    public $active = 1;


    public static function store()
    {
        return new DuAnDataStore();
    }
    public function rand_gia(): int
    {
        return CommonMethods::get_rand_value($this->text_gia, Variables::$step_gia);
    }

    public function rand_gia_dat_tho_cu(): int
    {
        return CommonMethods::get_rand_value($this->text_gia_dat_tho_cu, Variables::$step_gia);
    }
    public function rand_gia_dat_khac(): int
    {
        return CommonMethods::get_rand_value($this->text_gia_dat_khac, Variables::$step_gia);
    }

    public function rand_dien_tich(): int
    {
        return CommonMethods::get_rand_value($this->text_dien_tich, Variables::$step_dien_tich);
    }
    public function rand_dien_tich_dat_tho_cu(): int
    {
        return CommonMethods::get_rand_value($this->text_dien_tich_dat_tho_cu, Variables::$step_dien_tich);
    }
    public function rand_dien_tich_dat_khac(): int
    {
        return CommonMethods::get_rand_value($this->text_dien_tich_dat_khac, Variables::$step_dien_tich);
    }
    public function rand_chieu_ngang(): int
    {
        return CommonMethods::get_rand_value($this->text_chieu_ngang, Variables::$step_chieu_ngang);
    }
    public function dia_chis(): array
    {
        $vi_tris = json_decode($this->text_vi_tri);
        return array_map(fn ($vt) => $this->ten_duong . ", " . $vt, $vi_tris);
    }

    public function ten_loai(): string
    {
        return Variables::$loai[$this->loai];
    }

    public function tieu_des(): array
    {

        return json_decode($this->text_tieu_de, true);
    }

    public function get_text_tieu_des(): string
    {
        try {
            return CommonFunctions::array_to_string($this->tieu_des(), "\n");
        } catch (\Throwable $th) {
            //throw $th;
        }
        return "";
    }

    public function set_text_tieu_des(string $tieu_des)
    {

        try {
            $this->text_tieu_de = json_encode(CommonFunctions::string_to_array($tieu_des, "\n"));
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function link_checks(): array
    {
        return json_decode($this->link_checks, true);
    }

    public function get_link_checks(): string
    {
        try {
            return CommonFunctions::array_to_string($this->link_checks(), "\n");
        } catch (\Throwable $th) {
            //throw $th;
        }
        return "";
    }

    public function set_link_checks(string $link_checks)
    {

        try {
            $this->link_checks = json_encode(CommonFunctions::string_to_array($link_checks, "\n"));
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function vi_tris(): array
    {
        return json_decode($this->text_vi_tri, true);
    }

    public function get_text_vi_tris(): string
    {
        try {
            return CommonFunctions::array_to_string($this->vi_tris(), "\n");
        } catch (\Throwable $th) {
            //throw $th;
        }
        return "";
    }

    public function set_text_vi_tris(string $vi_tris)
    {

        try {
            $this->text_vi_tri = json_encode(CommonFunctions::string_to_array($vi_tris, "\n"));
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function tien_ichs(): array
    {
        try {
            return json_decode($this->text_tien_ich, true);
        } catch (\Throwable $th) {
            //throw $th;
        }
        return array();
    }


    public function build_dien_tich()
    {
        $obj = array();
        $maxLoopdien_tich = 10;
        $isStop = false;
        while (!$isStop && $maxLoopdien_tich > 0) {
            $obj["dien_tich_dat_tho_cu"] = 0;
            $obj["dien_tich_dat_khac"] = 0;
            if (CommonFunctions::string_is_not_null($this->text_dien_tich)) {
                $obj["dien_tich"] = $this->rand_dien_tich();
            } else {
                $obj["dien_tich_dat_tho_cu"] = $this->rand_dien_tich_dat_tho_cu();
                $obj["dien_tich_dat_khac"] = $this->rand_dien_tich_dat_khac();
                $obj["dien_tich"] =  $obj["dien_tich_dat_tho_cu"] +  $obj["dien_tich_dat_khac"];
            };
            $maxLoop = 10;
            while (!$isStop && $maxLoop > 0) {
                $obj["chieu_ngang"] = $this->rand_chieu_ngang() ?? CommonFunctions::array_rand_item([5, 7, 6, 9, 10]);
                // ngangs.removeAt(index);
                $obj["chieu_dai"] = round($obj["dien_tich"] / $obj["chieu_ngang"]);
                $ratio = $obj["chieu_dai"] / $obj["chieu_ngang"];
                if ($ratio >= 2 && $ratio <= 5) {
                    $isStop = true;
                }
                $maxLoop--;
            }
            $ratio = $obj["chieu_dai"] / $obj["chieu_ngang"];
            if (!($ratio >= 2 && $ratio <= 5)) {
                $maxLoopdien_tich--;
            }
        }

        return $obj;
    }
    public function build_gia()
    {
        $obj = array();
        $obj["gia_dat_tho_cu"] = 0;
        $obj["gia_dat_khac"] = 0;
        if (CommonFunctions::string_is_not_null($this->text_gia)) {
            $obj["gia"] = $this->rand_gia() * pow(10, 6);
        } else {
            $obj["gia_dat_tho_cu"] = $this->rand_gia_dat_tho_cu() * pow(10, 6);
            $obj["gia_dat_khac"] = $this->rand_gia_dat_khac() * pow(10, 6);
            $obj["gia"] = ($obj["gia_dat_tho_cu"] + $obj["gia_dat_khac"]) / 2;
        }
        return $obj;
    }
    public function check_links()
    {
        $links = $this->link_checks();
        if ($links == null && count($links) == 0) return;
        $data = array();
        foreach ($links as $link) {
            $host = parse_url($link)['host'];
            $idSite = array_search($host, Variables::$site);
            $item = array(
                "Link" => $link,
                "Name" => $this->ten_du_an ?? "Chưa xác định" . "($this->ma_du_an)",
                "IdSite" => $idSite
            );
            array_push($data, $item);
        }

        try {
            $query_url = Variables::$path_api . '/' . 'CheckLinkSite';
            $body = array("data" => $data);
            $res = CommonHttps::post($query_url,  $body);
            if ($res->isSuccess()) {
                $dataobj = $res->firstData();
                $name_du_ans = array_keys($dataobj);
                try {
                    foreach ($name_du_ans as $name_du_an) {
                        $objLink = $dataobj[$name_du_an];
                        $keys = array_keys($objLink);
                        foreach ($keys as $key) {
                            try {
                                $obj = $objLink[$key];
                                $item = new BDSLogsLinkSite(array(
                                    "id_du_an" => $this->id,
                                    "link" => $key,
                                    "ten_nguoi_dang" => $obj["TenNguoiDang"],
                                    "text_gia" => $obj["Gia"],
                                    "text_dien_tich" => $obj["DienTich"],
                                    "text_dien_thoai" => $obj["DienThoai"]
                                ));
                                $item->save();
                            } catch (Exception $e) {
                                CommonFunctions::console_log($e);
                            }
                        }
                    }
                } catch (Exception $e) {
                    CommonFunctions::console_log($e);
                }
            } else {
                CommonFunctions::alert($res->getMesage());
            }
        } catch (Exception $e) {
            CommonFunctions::console_log($e);
        }
    }

    public static function check_links_duan()
    {
        try {
            $duans = BDSDuAn::query()->where('active', 1)->find();
            foreach ($duans as $duan) {
                $duan->check_links();
            }
            return true;
        } catch (Exception $e) {
            CommonFunctions::console_log($e);
        }
        return false;
    }
    // gắn tạm
    public static function clear_chrome()
    {
        try {
            $query_url = Variables::$path_api . '/' . 'ClearChrome';
            $body = array();
            $res = CommonHttps::post($query_url,  $body);
            if (!$res->isSuccess()) {
                CommonFunctions::alert("Tài khoản hợp lệ");
            } else {
                CommonFunctions::alert($res->getMesage());
            }
        } catch (Exception $e) {
            CommonFunctions::console_log($e);
        }
    }

    public function up_drive()
    {
        if (CommonMethods::is_not_null($this->url_drive)) {
            try {
                $query_url = Variables::$path_api . "/FileDownloaderDrive";
                $body = array(
                    "Ma" => $this->ma_du_an,
                    "Name" => $this->ten_du_an,
                    "DiaChi" => $this->ten_duong,
                    "TenLoai" => Variables::$loai[$this->loai],
                    "UrlDrive" => $this->url_drive
                );
                $res = CommonHttps::post($query_url,  $body);

                return $res->isSuccess();
            } catch (Exception $e) {
                CommonFunctions::console_log($e);
            }
        }
        return false;
    }
}

function bds_del_duan()
{
    $ids = $_POST['ids'];
    $list = BDSDuAn::query()->where_in("id", $ids)->find();
    foreach ($list as $item) {
        $item->delete();
    };
    echo json_encode($ids);
    exit();
}
add_action('wp_ajax_nopriv_bds_del_duan', 'bds_del_duan');
add_action('wp_ajax_bds_del_duan', 'bds_del_duan');

function bds_up_drive_duan()
{
    $ids = $_POST['ids'];
    $list = BDSDuAn::query()->where_in("id", $ids)->find();
    foreach ($list as $item) {
        $item->up_drive();
    };
    echo json_encode($ids);
    exit();
}
add_action('wp_ajax_nopriv_bds_up_drive_duan', 'bds_up_drive_duan');
add_action('wp_ajax_bds_up_drive_duan', 'bds_up_drive_duan');
