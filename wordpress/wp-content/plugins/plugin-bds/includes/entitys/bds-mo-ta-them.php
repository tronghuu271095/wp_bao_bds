<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}
class BDSMoTaThem extends AbstractEntity
{
    public $id;
    public $noi_dung;
    public $nhom;
    public $is_top;
    public $is_ban_dat;
    public static function store()
    {
        return new MoTaThemDataStore();
    }
}
