<?php

/**
 * ThanhVien_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}
class BDSThanhVien extends AbstractEntity
{
    public $id;
    public $active = 1;
    public $ho_ten;
    public $email;
    public $dia_chi;
    public $dien_thoai;
    public static function store()
    {
        return new ThanhVienDataStore();
    }
    public function account(int $id_site)
    {
        $data = BDSThanhVienSite::query()->where('id_site', $id_site)->where('id_thanh_vien', $this->id)->find();
        if (count($data) >= 1) {
            return $data[0];
        }
        return null;
    }

    public static function nap_tien(int $kieu_nap, int $so_tien)
    {

        $data = array();
        $accounts = BDSThanhVienSite::query()->where('id_site', 1)->find();
        foreach ($accounts as $account) {
            $data[$account->ten_dang_nhap] = $so_tien;
        }
        $query_url = Variables::$path_api . '/' . 'Recharge';
        $body = array(
            "TenDangNhap" => "ryanle.bds@gmail.com",
            "MatKhau" => "@iI321321",
            "Data" =>  $data,
            "Kind" => $kieu_nap
        );
        $res = CommonHttps::post($query_url,  $body);
        if ($res->isSuccess()) {
            BDSThanhVienSite::update_balance_by_site(1);
        } else {
            CommonFunctions::console_log($res->getMesage());
        }
    }
    public function update_balance()
    {
        BDSThanhVienSite::update_balance_by_thanh_vien($this->id);
    }



    public function check_ThanhVien()
    {
        if (!($this->id > 0)) {
            CommonFunctions::alert("Dữ liệu không tồn tại");
            return;
        }

        if (!CommonFunctions::string_is_not_null($this->ten_dang_nhap)) {
            CommonFunctions::alert("Tên đăng nhập không được bỏ trống");
            return;
        }

        if (!CommonFunctions::string_is_not_null($this->mat_khau)) {
            CommonFunctions::alert("Mật khẩu không được bỏ trống");
            return;
        }

        if (!($this->id_site > 0)) {
            CommonFunctions::alert("Web site không được bỏ trống");
            return;
        }

        $query_url = Variables::$path_api . '/' . 'CheckThanhVien';
        $body = array(
            "TenDangNhap" => $this->ten_dang_nhap,
            "MatKhau" => $this->mat_khau,
            "IdSite" => $this->id_site

        );
        $res = CommonHttps::post($query_url,  $body);
        if (!$res->isSuccess()) {
            CommonFunctions::alert("Tài khoản hợp lệ");
        } else {
            $this->ghi_chu = $res->getMesage();
            $this->active = 0;
            $this->save();
            CommonFunctions::alert($res->getMesage());
        }
    }
}

function bds_del_thanh_vien()
{
    $ids = $_POST['ids'];
    $list = BDSThanhVien::query()->where_in("id", $ids)->find();
    foreach ($list as $item) {
        $item->delete();
    };
    echo json_encode($ids);
    exit();
}
add_action('wp_ajax_nopriv_bds_del_thanh_vien', 'bds_del_thanh_vien');
add_action('wp_ajax_bds_del_thanh_vien', 'bds_del_thanh_vien');
