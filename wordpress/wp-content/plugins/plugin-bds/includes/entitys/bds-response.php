<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}
class BDSResponse
{
    protected $dataResult;
    protected $gotoLink;
    protected $intResult;
    protected $message;
    protected $pagination;
    protected $propertyNameList;
    protected $strResult;
    // public function properties()
    // {
    //     return get_object_vars($this);
    // }
    public function __construct(array $properties = array())
    {
        foreach ($properties as $key => $value) {
            $this->{$key} = $value;
        }
    }
    public function dataResult()
    {
        return $this->dataResult ?? array();
    }
    public function isSuccess()
    {
        return $this->intResult > 0;
    }

    public function firstData()
    {
        if ($this->dataResult != null && count($this->dataResult) > 0)
            return $this->dataResult[0];
        else
            return null;
    }

    public function getMesage()
    {
        return $this->message;
    }
}
