<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}
class BDSKieuNha extends AbstractEntity
{

    public $id;
    public $ten_kieu_nha;
    public $text_so_phong;
    public $text_so_tang;
    public $text_so_toilet;
    public $text_ket_cau;
    public static function store()
    {
        return new KieuNhaDataStore();
    }

    public function ket_caus(): array
    {
        return json_decode($this->text_ket_cau);
    }
    public static function get_rand(): BDSKieuNha
    {
        return CommonFunctions::array_rand_item(BDSKieuNha::all());
    }

    public function rand_data(): array
    {
        $so_phong_ngu = CommonMethods::get_rand_value($this->text_so_phong, 1);
        $so_toilet = CommonMethods::get_rand_value($this->text_so_toilet, 1);
        $so_tang = CommonMethods::get_rand_value($this->text_so_tang, 1);
        $ket_cau = CommonFunctions::array_rand_item($this->ket_caus());
        return array(
            "id" => $this->id,
            "so_phong_ngu" => $so_phong_ngu,
            "so_toilet" => $so_phong_ngu < $so_toilet ? $so_phong_ngu : $so_toilet,
            "so_tang" => $so_tang,
            "ket_cau" => $ket_cau,
        );
    }
}
