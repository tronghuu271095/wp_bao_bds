<?php

/**
 * ThanhVienSite_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}
class BDSThanhVienSite extends AbstractEntity
{
    public $id;
    public $id_site;
    public $id_thanh_vien;
    public $balance;
    public $ten_dang_nhap;
    public $mat_khau;
    public $ghi_chu;

    public static function store()
    {
        return new ThanhVienSiteDataStore();
    }

    public function update_balance()
    {
        if ($this->id_site == 3) return true;
        $query_url = Variables::$path_api . '/' . 'GetBalanceInfo';
        $body = array(
            "TenDangNhap" => $this->ten_dang_nhap,
            "MatKhau" => $this->mat_khau,
            "IdSite" => $this->id_site
        );
        $res = CommonHttps::post($query_url,  $body);
        if ($res->isSuccess()) {
            $data = $res->firstData();
            $this->balance = $data["tkChinh"] + $data["tkkM1"] + $data["tkkM2"];
            $this->ghi_chu = $res->getMesage();
            $this->save();
        } else {
            $this->ghi_chu = $res->getMesage();
            $this->save();
            CommonFunctions::console_log($res->getMesage());
        }
        return $res->isSuccess();
    }

    public static function update_balance_by_site($id_site)
    {
        try {
            $thanhvien_sites = BDSThanhVienSite::query()->where("id_site", $id_site)->find();
            foreach ($thanhvien_sites as $thanhvien_site) {
                $thanhvien_site->update_balance();
            }
            return true;
        } catch (\Throwable $th) {
            //throw $th;
        }
        return false;
    }

    public static function update_balance_by_thanh_vien($id_thanh_vien): bool
    {
        try {
            $thanhvien_sites = BDSThanhVienSite::query()->where("id_thanh_vien", $id_thanh_vien)->find();
            foreach ($thanhvien_sites as $thanhvien_site) {
                $thanhvien_site->update_balance();
            }
            return true;
        } catch (\Throwable $th) {
            //throw $th;
        }
        return false;
    }

    public static function update_all_balance()
    {
        try {
            $thanhvien_sites = BDSThanhVienSite::all();
            foreach ($thanhvien_sites as $thanhvien_site) {
                $thanhvien_site->update_balance();
            }
            return true;
        } catch (\Throwable $th) {
            //throw $th;
        }
        return false;
    }
}
