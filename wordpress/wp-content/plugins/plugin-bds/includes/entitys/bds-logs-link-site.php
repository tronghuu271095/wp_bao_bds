<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}
class BDSLogsLinkSite extends AbstractEntity
{
    public $id;
    public $id_du_an;
    public $link;
    public $ten_nguoi_dang;
    public $text_gia;
    public $text_dien_tich;
    public $text_dien_thoai;
    public $ngay_tao;
    public function save()
    {
        if (!$this->ngay_tao) {
            $this->ngay_tao = new DateTime('NOW', new DateTimeZone('+0700'));
        }
        parent::save();
    }

    public static function store()
    {
        return new LogsLinkSiteDataStore();
    }

    public function du_an(): BDSDuAn
    {
        return BDSDuAn::find_one($this->id_du_an);
    }
}
