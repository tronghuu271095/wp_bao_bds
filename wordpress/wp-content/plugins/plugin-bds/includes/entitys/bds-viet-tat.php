<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}
class BDSVietTat extends AbstractEntity
{
    public $id;
    public $tu_goc;
    public $tu_thay_the;
    public static function store()
    {
        return new VietTatDataStore();
    }
    public function tu_thay_the(): array
    {
        return json_decode($this->tu_thay_the);
    }

    public function rand_tu_thay_the(): string
    {
        return CommonFunctions::array_rand_item($this->tu_thay_the());
    }
}
