<?php

/**
 * Account_DataStore class file.
 *
 * @package \Classes
 */

if (!defined('ABSPATH')) {
    exit;
}
class BDSThuocTinh extends AbstractEntity
{
    public $id;
    public $ten_thuoc_tinh;
    public $ma_thuoc_tinh;
    public $noi_dung;
    public static function store()
    {
        return new ThuocTinhDataStore();
    }
    public function data_range()
    {
        return json_decode($this->noi_dung);
    }
    public static function get_data($ma_thuoc_tinh,  $arrMaxLength = array(3000), $value = ""): array
    {
        $data = BDSThuocTinh::find_one_by("ma_thuoc_tinh", $ma_thuoc_tinh);
        $data_range = $data->data_range();
        $data = CommonMethods::create_data($data_range, $arrMaxLength, $value);
        return $data;
    }

    public static function get_data_tieu_de($arrMaxLength = array(99), $value = ""): array
    {
        return BDSThuocTinh::get_data("tieu_de", $arrMaxLength, $value);
    }
    public static function get_data_vi_tri($arrMaxLength = array(3000), $value = ""): array
    {
        return BDSThuocTinh::get_data("vi_tri", $arrMaxLength, $value);
    }
    public static function get_data_dien_tich($arrMaxLength = array(3000), $value = ""): array
    {
        return BDSThuocTinh::get_data("dien_tich", $arrMaxLength, $value);
    }
    public static function get_data_gia($arrMaxLength = array(3000), $value = ""): array
    {
        return BDSThuocTinh::get_data("gia", $arrMaxLength, $value);
    }
    public static function get_data_hien_trang($arrMaxLength = array(3000), $value = ""): array
    {
        return BDSThuocTinh::get_data("hien_trang", $arrMaxLength, $value);
    }
    public static function get_data_thong_tin_phap_ly($arrMaxLength = array(3000), $value = ""): array
    {
        return BDSThuocTinh::get_data("thong_tin_phap_ly", $arrMaxLength, $value);
    }
}
