<?php

/**
 * BDS book common functions.
 *
 * @package BDS\Common
 */
class Variables
{
    public static $plugin_name = 'bds';
    public static $cache_du_an = array();
    public static $cache_kieu_nha = array();
    public static string $path_api = 'http://103.77.167.179:4200/api/v1/dangtin'; //https://localhost:44363/
    public static int $max_dequy = 100;
    public static int $max_length_tieu_de = 99;
    public static int $max_length_mo_ta = 3000;
    public static int $max_build = 10;
    public static int $max_check_trang_thai_tin = 20;
    public static float $step_gia = 0.1;
    public static float $step_dien_tich = 0.1;
    public static float $step_chieu_ngang = 0.1;
    public static array $group_tien_ich_required = array(6);
    public static $char_starts = array('', '*', '.', '+', '-', '* ', '. ', '+ ', '- ');
    public static array $group_tien_ich = array(1, 2, 3, 4, 5, 6, 7);
    public static $text_lien_he = array(
        'Liên hệ: %dienthoai%.',
        'Quan tâm gọi ngay %dienthoai%.',
        'Quan tâm hãy gọi %dienthoai%.',
        'Gọi %dienthoai% để xem ngay...',
        'Liên hệ ngay %dienthoai%',
        'Liên hệ xem đất trước 30 phút.',
        'Quý khách có nhu cầu vui lòng Liên hệ số điện thoại %dienthoai%',
        'Liên hệ trực tiếp chính chủ:%dienthoai%',
        'Liên hệ: %dienthoai% (Zalo/Mess/Viber)',
        'LH: %dienthoai% để xem sổ và vị trí.',
        'LH: %dienthoai% để xem vị trí',
        'Để kiểm tra pháp lý',
        'Rất mong làm việc với khách thiện chí.'
    );
    public static $text_huong_nhas = array(
        'Căn nhà hướng %huongnha%.',
        'Hướng nhà %huongnha%.',
        'Nhà hướng %huongnha%.',
        'Hướng chính %huongnha%',
        'Hướng %huongnha%',
        'Hướng cửa %huongnha%'
    );
    public static $text_dien_tichs = array(
        'ngang %chieungang%x dài %chieudai%= %dientich%',
        'chiều ngang %chieungang% x chiều dài %chieudai% = %dientich%',
        '%dientich%',
        '%dientich% ngang %chieungang% x dài %chieudai%',
        '%chieungang% x %chieudai% = %dientich%'
    );
    public static $text_dien_tich_phus = array(
        'trong đó có %dientichdatthocu% thổ cư',
        'trong đó %dientichdatthocu% thổ cư.',
        'gồm %dientichdatthocu% thổ cư.',
        'có %dientichdatthocu% thổ',
        '%dientichdatthocu% thổ cư',
        'có sẵn %dientichdatthocu% thổ cư.',
        '%dientichdatthocu% thổ cư',
        'trong đó có %dientichdatthocu% đất thổ cư',
        'trong đó %dientichdatthocu% đất thổ cư',
        'gồm %dientichdatthocu% đất thổ cư.',
        'có %dientichdatthocu% đất thổ cư',
        '%dientichdatthocu% đất thổ cư',
        'có sẵn %dientichdatthocu% đất thổ cư.',
        '%dientichdatthocu% đất thổ cư',
        'trong đó có %dientichdatthocu% thổ',
        'trong đó %dientichdatthocu% thổ',
        'gồm %dientichdatthocu% thổ',
        'có %dientichdatthocu% thổ',
        '%dientichdatthocu% thổ',
        'có sẵn %dientichdatthocu% thổ',
        '%dientichdatthocu% thổ',
        'trong đó có %dientichdatthocu% đất ở',
        'trong đó %dientichdatthocu% đất ở',
        'gồm %dientichdatthocu% đất ở',
        'có %dientichdatthocu% đất ở',
        '%dientichdatthocu% đất ở',
        'có sẵn %dientichdatthocu% đất ở',
        '%dientichdatthocu% đất ở',
    );
    public static $text_gia_dat_tho_cus = array(
        'giá đất thổ cư chỉ %giadatthocu%',
        'đất thổ cư giá chỉ %giadatthocu%',
        'đất thổ cư giá %giadatthocu%',
        'thổ cư giá tốt %giadatthocu%',
        'giá chỉ %giadatthocu% đất thổ cư',
        'đất thổ giá %giadatthocu%',
        'đất thổ giá chỉ %giadatthocu%',
        'đất thổ giá tốt %giadatthocu%',
        'đất thổ giá rẻ %giadatthocu%',
        'đất ở giá chỉ %giadatthocu%',
        'đất ở giá %giadatthocu%',
        'trong đó giá đất thổ cư %giadatthocu%',
        'trong đó giá đất thổ %giadatthocu%',
        'trong đó giá đất ở %giadatthocu%',
        'đất thổ cư giá quá tốt %giadatthocu%',
        'đất thổ cư chỉ %giadatthocu% giá tốt nhất trong khu vực.',
        'đất thổ chỉ %giadatthocu% giá tốt nhất trong khu vực.',
        'đất ở chỉ %giadatthocu% giá tốt nhất trong khu vực.',
        'đất thổ cư chỉ %giadatthocu% giá tốt nhất ở khu này.',
        'đất thổ chỉ %giadatthocu% giá tốt nhất ở khu này.',
        'đất ở chỉ %giadatthocu% giá tốt nhất ở khu này.',
        'thổ cư giá chỉ %giadatthocu% .'
    );

    public static $loai_tai_khoan = array(
        0 => "TK Chính",
        1 => "TK KM 1",
        2 => "TK KM 2",

    );
    public static $huong_nha = array(
        1 => "Đông",
        2 => "Tây",
        3 => "Nam",
        4 => "Bắc",
        5 => "Đông-Bắc",
        6 => "Tây-Bắc",
        7 => "Tây-Nam",
        8 => "Đông-Nam"
    );

    public static $site = array(
        1 => "batdongsan.com.vn",
        2 => "dothi.net",
        3 => "bannhasg.com",
        4 => "alonhadat.com.vn",
    );

    public static $loai = array(
        1 => "Bán đất",
        2 => "Bán nhà mặt phố",
        3 => "Bán nhà riêng",
    );
    public static $capability = 'manage_options';
    public static function set_var(array $data)
    {
        static::$path_api = $data['path_api'] ?? static::$path_api;
        static::$max_length_tieu_de = $data['max_length_tieu_de'] ?? static::$max_length_tieu_de;
        static::$max_length_mo_ta = $data['max_length_mo_ta'] ?? static::$max_length_mo_ta;
        static::$max_build = $data['max_build'] ?? static::$max_build;
        static::$max_check_trang_thai_tin = $data['max_check_trang_thai_tin'] ?? static::$max_check_trang_thai_tin;
        static::$step_gia = $data['step_gia'] ?? static::$step_gia;
        static::$step_dien_tich = $data['step_dien_tich'] ?? static::$step_dien_tich;
        static::$step_chieu_ngang = $data['step_dien_tich'] ?? static::$step_chieu_ngang;
    }
    // không được dùng biến truyền vô các hàm __() translation vì khi dùng tool bên thứ 3, sẽ ko dịch được biến thành 'giftbook', phải truyền trực tiếp 'giftbook' vô
    // public static $text_domain = 'bds';
}
