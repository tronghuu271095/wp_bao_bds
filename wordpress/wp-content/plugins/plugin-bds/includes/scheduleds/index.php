<?php

add_action('wpb_cap_nhat_balance', 'wpb_cap_nhat_balance_func');

function wpb_cap_nhat_balance_func()
{
    BDSThanhVienSite::update_all_balance();
}

add_action('wpb_check_links_duan', 'wpb_check_links_duan_func');

function wpb_check_links_duan_func()
{
    //tronghuu95 để tránh nữa đêm mà báo! không có ý nghĩa vào nữa đêm
    $now = new DateTime('NOW', new DateTimeZone('+0700'));
    $hour = $now->format('H');
    if ($hour >= 6 && $hour <= 22) {
        BDSDuAn::check_links_duan();
    }
}
add_action('wpb_clear_chrome', 'wpb_clear_chrome_func');

function wpb_clear_chrome_func()
{
    CommonMethods::clear_chrome();
}

add_action('wpb_cap_nhat_trang_thai', 'wpb_cap_nhat_trang_thai_func');

function wpb_cap_nhat_trang_thai_func()
{
    try {
        $data = BDSTinDang::query()->where_gte("id_tin", 1)->where("trang_thai", 1)->limit(Variables::$max_check_trang_thai_tin)->sort_by('ngay_tao')->order('DESC')->find();
        foreach ($data as $item) {
            $item->cap_nhat_trang_thai();
        }
    } catch (\Throwable $th) {
    }
}



add_action('wpb_bds_post_auto', 'wpb_bds_post_auto_func');

function wpb_bds_post_auto_func()
{
    try {
        $data = BDSTinDang::query()->is_null("id_tin")->str_query('`ngay_dang` is not null && `ngay_dang` = CURDATE() ')->sort_by('ngay_tao')->order('DESC')->find();
        foreach ($data as $item) {
            $item->post();
        }
    } catch (\Throwable $th) {
    }
}
