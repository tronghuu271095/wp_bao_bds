<?php
class Plugin_BDS_Deactivator
{

	public static function deactivate()
	{
		// Stores::destroy(); // gắn tạm vào thôi
		remove_action('admin_menu', 'addActionBDS');
		remove_action('wp_footer', 'addTemplateFooter');
		remove_action('rest_api_init', 'register');
	}
}
